<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');


$fname = $_POST['fname'];
$lname = $_POST['lname'];
$address = $_POST['address'];
$email = $_POST['email'];
$phone_number = $_POST['phone_number'];
$invoice_id = $_POST['invoice_id'];
$item_price = $_POST['item_price'];
$item_name = $_POST['item_name'];
$quantity = $_POST['quantity'];
$total_price = $_POST['total_price'];
$item_sub_total_price = $_POST['item_sub_total_price'];
$vat = $_POST['vat'];
$disCount = $_POST['disCount'];
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");


$data = [
        'fname'=>$fname,
        'lname'=>$lname,
        'address'=>$address,
        'email'=>$email,
        'phone_number'=>$phone_number,
        'invoice_id'=>$invoice_id,
        'item_price'=>$item_price,
        'item_name'=>$item_name,
        'quantity'=>$quantity,
        'total_price'=>$total_price,
        'item_sub_total_price'=>$item_sub_total_price,
        'vat'=>$vat,
        'disCount'=>$disCount,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    
    ];

$pdo = connectDB();

$sql = "INSERT INTO `billing_details` (                                             
                             `invoice_id`,    
                             `item_name`,    
                             `item_price`, `quantity`,
                              `total_price`, 
                              `item_sub_total_price`,
                               `vat`, `disCount`, `fname`, 
                               `lname`, `address`,
                                `email`, `phone_number`,
                                 `created_at`, `updated_at`) 
                     VALUES (
                             :invoice_id,  
                             :item_name,  
                             :item_price,  
                             :quantity,  
                             :total_price,  
                             :item_sub_total_price,  
                             :vat,  
                             :disCount,  
                             :fname,  
                             :lname,
                             :address,
                             :email,
                             :phone_number, 
                             :created_at,
                             :updated_at
                             )";

$result = insert($sql, $data);

if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
// print_r( $_SESSION); 

 redirect('./frontend/thankyou.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";

//   redirect('pdfgenerator.php');

}