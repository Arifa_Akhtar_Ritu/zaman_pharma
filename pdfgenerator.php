<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/config.php');
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$pdo = connectDb();

$sql = "SELECT * FROM `orders` WHERE customer_id = :customer_id";

$stmt = $pdo->prepare($sql);
$stmt->execute(['customer_id' => $_SESSION['anonymous_user']]);

$invoiceData = $stmt->fetchAll();

if(!count($invoiceData)){
	$mpdf = new \Mpdf\Mpdf();
	$mpdf->WriteHTML('');
	$mpdf->Output();
}

$counter_s = 0;
$subtotal_s = 0;
$total_s = 0;
foreach ($invoiceData as $value) {
	$counter_s++;
	$subtotal_s += $value['item_sub_total_price'];
	$disCount = ($subtotal_s / 100) * 10;
	$netTotal = $subtotal_s - $disCount;
	$vat = ($netTotal / 100) * 15;
	$total_s = ($netTotal + $vat);
}

$invoiceNumber = $invoiceData[0]['invoice_id'];
$createdAt = $invoiceData[0]['created_at'];
$address = $invoiceData[0]['address'];
$customerName = $invoiceData[0]['fname'] . ' ' . $invoiceData[0]['lname'];
$customerPhoneNumber = $invoiceData[0]['phonenumber'];
$customerEmail = $invoiceData[0]['email'];
$paymentCreatedAt = $invoiceData[0]['created_at'];


$counter = 0;
$subtotal =0;
// $total = 0;

$tr = '';
foreach($invoiceData as $itemValue):


	$counter++;
	$subtotal += $itemValue['item_sub_total_price'];
	$disCount = ($subtotal/100)*10;
	$netTotal =$subtotal-$disCount;
	$vat =($netTotal/100)*15;
	$total = ($netTotal+$vat);

	$tr .= '<tr>';
	$tr .= '<td class="text-center">'.$counter.'</td>';
	$tr .= '<td>'.$itemValue['m_name'].'</td>';
	$tr .= '<td class="text-right">'.$itemValue['m_quantity'].'</td>';
	$tr .= '<td class="text-right">'.$itemValue['m_price'].'</td>';
	$tr .= '<td class="text-right">'.$itemValue['item_sub_total_price'].' BDT</td>';
	$tr .= '</tr>';
endforeach;

// echo $tr;

// die();


		$htmloutput = <<<PDF

<div class="row">
  <div class="col-sm-12">
	  	<div class="panel panel-default invoice" id="invoice">
		  <div class="panel-body">
			<div class="invoice-ribbon"><div class="ribbon-inner">PAID</div></div>
		    <div class="row">

				<div class="col-sm-4 top-left">
					<!-- <i class="fa fa-rocket"></i> -->
					<img src="./admin/dist/img/logo.png" width="100px" height="100px">
				</div>
                <div class="col-md-4 top-middle">
                <h1> Zaman Pharmacy</h1>
                </div>
				<div class="col-sm-4 top-right">
						<h3 class="marginright">INVOICE-#$invoiceNumber</h3>
                        
						<span class="marginright">$createdAt</span>
			    </div>

			</div>
			<hr>
			<div class="row">
				<div class="col-sm-4 from" style="width:300px; float:left">
					<p class="lead marginbottom">From : Zaman Pharmacy</p>
					<p>Address:</p>
					<p>House-35,Road-10,Sector-10,<br> Uttara,Dhaka.</p>
					<p>Phone: 01608637314</p>
					<p>Email: arifaritu7@gmail.com</p>
				</div>

				<div class="col-sm-4 to" style="width:200px">
					<p class="lead marginbottom">To :<strong>$customerName</strong></p>
					<p>Address: $address</p>
					<p>Phone: $customerPhoneNumber</p>
					<p>Email: $customerEmail</p>

			    </div>

			    <div class="col-sm-4">
					<p class="lead marginbottom payment-info">Payment details</p>
					<p>Date: $paymentCreatedAt</p>
					<p>Total Amount: $total_s BDT</p>
			    </div>

			</div>

			<div class="row table-row">
				<table style="border-collapse: collapse;width: 100%;" border="1">
			      <thead>
			        <tr style="border:1px solid">
			          <th class="text-center" style="width:5%">Ser No</th>
			          <th style="width:50%">Item</th>
			          <th class="text-right" style="width:15%">Quantity</th>
			          <th class="text-right" style="width:15%">Unit Price</th>
			          <th class="text-right" style="width:15%">Total Price</th>
			        </tr>
			      </thead>	
				  <tbody>
				  	$tr 
				  </tbody>			
         
			    </table>

			</div>

			<div class="row">
			<div class="col-md-6 margintop">
				<p class="lead marginbottom">THANK YOU!</p>

				
			</div>
			<div class="col-md-6 text-right pull-right invoice-total" style="padding-left: 400px;">
				<p>Subtotal : $subtotal BDT</p>
				<p>Discount (10%) : $disCount BDT</p>
				<p>VAT (15%) : $vat BDT</p>
				<p>Total : $total BDT</p>
			</div>
			</div>
			</div>
			</div>
			</div>
		</div>
	 </div>

PDF;

$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML($htmloutput);
$mpdf->Output();

unset($_SESSION['anonymous_user']);
