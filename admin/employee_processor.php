<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$e_fname = $_POST['e_fname'];
$e_lname = $_POST['e_lname'];
$email = $_POST['email'];
$password = $_POST['password'];
$address = $_POST['address'];
$contactno = $_POST['contactno'];
$gender = $_POST['gender'];
$marital = $_POST['marital'];
$created_at = $_POST['created_at'];
$updated_at = $_POST['updated_at'];

$data = [
        'e_fname'=>$e_fname,
        'e_lname'=>$e_lname,
        'email'=>$email,
        'password'=>$password,
        'address'=>$address,
        'contactno'=>$contactno,
        'gender'=>$gender,
        'marital'=>$marital,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    ];
$pdo = connectDB();


$sql = "INSERT INTO `employee`( `e_fname`,
                               `e_lname`,
                               `address`,
                                `email`,
                                `password`,
                                 `contactno`,
                                 `gender`,
                                 `marital`,
                                 `created_at`,
                                `updated_at`
                                 ) 
                            VALUES (
                                    :e_fname, 
                                    :e_lname,
                                    :address,
                                    :email,
                                    :password,
                                    :contactno, 
                                    :gender,
                                    :marital,
                                    :created_at,
                                    :updated_at)";

$result = insert($sql, $data);
if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
  redirect('manage_employee.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";

  //redirect('add_new_customer.php');

}