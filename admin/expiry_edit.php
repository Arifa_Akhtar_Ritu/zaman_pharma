<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');?>
<?php 

    $id = $_GET['id'];
    $data = ['id'=>$id];
    $pdo = connectDB();
    $sql = "SELECT * FROM `expiredate` WHERE id = :id";

try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){// true means: execution of query is successful
      $data = $stmt->fetch();

    }
  }catch(Exception $e){
    echo $e->getMessage();
  }
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

 

  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">New Expired date</h1>
            <p>Add Expiry Date</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
           <!-- Change End Plz -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
           <form action="/admin/expiry_update_processor.php" method="post"    name="">
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <div class="p-3 p-lg-5 border">
                        <div class="form-group row">
                        <div class="col-md-6">
                            <label for="mname"  class="text-black">Medicine Name <span class="text-danger">*</span></label>
                            <input name="mname" value="<?= $data['mname'] ?>" type="text" class="form-control" id="mname" >
                        </div>
                        <div class="col-md-6">
                            <label for="mfd" class="text-black">Manufacture Date <span class="text-danger">*</span></label>
                            <input name="mfd" value="<?= $data['mfd'] ?>" type="date" class="form-control" id="mfd" >
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exd" class="text-black">Expired Date <span class="text-danger">*</span></label>
                            <input  name="exd" value="<?= $data['exd'] ?>" type="date" class="form-control" id="exd"placeholder="">
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Update</button>
                        </div>
                        </div>
                    </div>
            </form>
           </div>
       </div>
    
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
