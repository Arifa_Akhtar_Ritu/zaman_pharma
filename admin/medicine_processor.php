<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
$medicinename = $_POST['medicinename'];
$genericname = $_POST['genericname'];
$description = $_POST['description'];
$packing = $_POST['packing'];
$quantity = $_POST['quantity'];
$price = $_POST['price'];
$price_unit = $_POST['price_unit'];
$supplier = $_POST['supplier'];
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");


$filename = time().'_'. $_FILES['image']['name'];
$target = $_FILES['image']['tmp_name'];
$destination = $_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$filename;

$is_upload_successfull = move_uploaded_file($target,$destination); 


$data = [
        'medicinename'=>$medicinename,
        'genericname'=>$genericname,
        'description'=>$description,
        'packing'=>$packing,
        'quantity'=>$quantity,
        'price'=>$price,
        'price_unit'=>$price_unit,
        'supplier'=>$supplier,
        'image'=>$filename,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    ];
$pdo = connectDB();


$sql = "INSERT INTO `medicine`( `medicinename`,
                               `genericname`,
                               `description`,
                               `packing`,
                               `quantity`,
                               `price`,
                               `price_unit`,
                                `supplier`,
                                `image`,
                                `created_at`,
                                `updated_at`
                                ) 
                            VALUES (
                                    :medicinename, 
                                    :genericname,
                                    :description,
                                    :packing,
                                    :quantity,
                                    :price,
                                    :price_unit,
                                    :supplier,
                                    :image,
                                    :created_at,
                                    :updated_at
                                    )";

$result = insert($sql, $data);
if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
 redirect('manage_medicine.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";

  //redirect('add_new_customer.php');

}