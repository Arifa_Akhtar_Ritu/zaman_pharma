<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');?>
<?php 

    $id = $_GET['id'];
    $data = ['id'=>$id];
    $pdo = connectDB();
    $sql = "SELECT * FROM `suppliers` WHERE id = :id";

try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){// true means: execution of query is successful
      $data = $stmt->fetch();

    }
  }catch(Exception $e){
    echo $e->getMessage();
  }
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

 

  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit Supplier</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
           <!-- Change End Plz -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
           <form action="/admin/supplier_update_processor.php" method="post"    name="">
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <div class="p-3 p-lg-5 border">
                        <div class="form-group row">
                        <div class="col-md-6">
                            <label for="sfname"  class="text-black">First Name <span class="text-danger">*</span></label>
                            <input name="sfname" value="<?= $data['sfname'] ?>" type="text" class="form-control" id="sfname" >
                        </div>
                        <div class="col-md-6">
                            <label for="slname" class="text-black">Last Name <span class="text-danger">*</span></label>
                            <input name="slname" value="<?= $data['slname'] ?>" type="text" class="form-control" id="slname" >
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="contactno" class="text-black">Contact No <span class="text-danger">*</span></label>
                            <input name="contactno" value="<?= $data['contactno'] ?>" type="tel" class="form-control" id="contactno"  placeholder="">
                        </div> 
                    </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="address" class="text-black">Address <span class="text-danger">*</span></label>
                            <input name="address" value="<?= $data['address'] ?>" type="address" class="form-control" id="address"  placeholder="">
                        </div> 
                    </div>
                
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="email" class="text-black">Email <span class="text-danger">*</span></label>
                            <input  name="email" value="<?= $data['email'] ?>" type="email" class="form-control" id="email"placeholder="">
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="password" class="text-black">Password <span class="text-danger">*</span></label>
                            <input name="password" value="<?= $data['password'] ?>"  type="password" class="form-control" id="password" placeholder="put your secret password">
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="companyname" class="text-black">Company Name <span class="text-danger">*</span></label>
                            <input name="companyname" value="<?= $data['companyname'] ?>"  type="text" class="form-control" id="companyname" placeholder="">
                        </div>
                        </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Update</button>
                        </div>
                    </div>
                    </div>
            </form>
           </div>
       </div>
    
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
