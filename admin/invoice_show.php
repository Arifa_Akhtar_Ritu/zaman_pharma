<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');
  include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
  require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
 
  
  
  $invoice_id = $_GET['invoice_id'];
 
  $pdo = connectDB();

$sql = "SELECT * FROM `orders` WHERE invoice_id = $invoice_id";

$stmt = $pdo->prepare($sql);
$stmt->execute();

$invoiceData = $stmt->fetchAll(PDO::FETCH_ASSOC);
// dd($invoiceData);


?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Invoice Data</h1>
            <p>Invoice Data Show</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="container bootstrap snippets bootdeys">
<div class="row">
  <div class="col-md-12">
	  	<div class="panel panel-default invoice" id="invoice">
		  <div class="panel-body p-5">
			<div class="invoice-ribbon"><div class="ribbon-inner">PAID</div></div>
		    <div class="row">

				<div class="col-sm-4 top-left">
					<!-- <i class="fa fa-rocket"></i> -->
					<img src="./dist/img/logo.png" width="100px" height="100px">
				</div>
                <div class="col-md-4 top-middle">
                <h1> Zaman Pharmacy</h1>
                </div>
				<div class="col-sm-4 top-right">
						<h3 class="marginright">INVOICE-#<?php echo $invoiceData[0]['invoice_id'];?></h3>
                        
						<span class="marginright"><?php echo $invoiceData[0]['created_at'];?></span>
			    </div>

			</div>
			<hr>
			<div class="row">
				<div class="col-sm-4 from">
					<p class="lead marginbottom">From : Zaman Pharmacy</p>
					<p>Address:</p>
					<p>House-35,Road-10,Sector-10,Uttara,Dhaka.</p>
					<p>Phone: 01608637314</p>
					<p>Email: arifaritu7@gmail.com</p>
				</div>

				<div class="col-sm-4 to">
					<p class="lead marginbottom">To :<strong> <?php echo $invoiceData[0]['fname'].' '.$invoiceData[0]['lname'];?></strong></p>
					<p>Address: <?php echo $invoiceData[0]['address'];?></p>
					<p>Phone: <?php echo $invoiceData[0]['phonenumber'];?></p>
					<p>Email: <?php echo $invoiceData[0]['email'];?></p>

			    </div>

			    <div class="col-sm-4 text-right payment-details">
					<p class="lead marginbottom payment-info">Payment details</p>
					<p>Date: <?php echo $invoiceData[0]['created_at'];?></p>
					
			    </div>

			</div>

			<div class="row table-row">
				<table class="table table-striped">
			      <thead>
			        <tr>
			          <th class="text-center" style="width:5%">Ser No</th>
			          <th style="width:50%">Item</th>
			          <th class="text-right" style="width:15%">Quantity</th>
			          <th class="text-right" style="width:15%">Unit Price</th>
			          <th class="text-right" style="width:15%">Total Price</th>
			        </tr>
			      </thead>
			      <tbody>
            <?php
              $counter = 0;
              $subtotal =0;
              // $total = 0;
                    foreach($invoiceData as $itemValue):
                        $counter++;
                        $subtotal += $itemValue['item_sub_total_price'];
                        $disCount = ($subtotal/100)*10;
                        $netTotal =$subtotal-$disCount;
                        $vat =($netTotal/100)*15;
                        $total = ($netTotal+$vat);

                  ?>
                  <tr>
                    <td class="text-center"><?php echo $counter?></td>
                    <td><?php echo $itemValue['m_name'];?></td>
                    <td class="text-right"> <?php echo $itemValue['m_quantity'];?> </td>
                    <td class="text-right"><?php echo $itemValue['m_price'];?> BDT</td>
                    <td class="text-right"><?php echo $itemValue['item_sub_total_price'];?> BDT</td>
                  </tr>
              <?php
                endforeach;
              ?> 
    
			       </tbody>
			    </table>

			</div>

			<div class="row">
			<div class="col-md-6 margintop">
				<p class="lead marginbottom">THANK YOU!</p>
			</div>
			<div class="col-md-6 text-right pull-right invoice-total">
					  <p>Subtotal : <?php echo $subtotal;?> BDT</p>
			          <p>Discount (10%) : <?php echo $disCount;?> BDT</p>
			          <p>VAT (15%) : <?php echo $vat;?> BDT</p>
			          <p>Total : <?php echo $total;?> BDT</p>
			</div>
			</div>
		  </div>
		</div>
	</div>
</div>
</div>
        </div>
   
     
     
       
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
