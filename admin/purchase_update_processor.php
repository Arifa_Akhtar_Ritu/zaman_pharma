<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
$id = $_POST['id'];
$invoiceno = $_POST['invoiceno'];
$sname = $_POST['sname'];
$paytype = $_POST['paytype'];
$date = $_POST['date'];
$medicinename = $_POST['medicinename'];
$quantity = $_POST['quantity'];
$mrp = $_POST['mrp'];
$amount = $_POST['amount'];
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");

//sanitization
    $data = [
         'id'=>$id,
        'invoiceno'=>$invoiceno,
        'sname'=>$sname,
        'paytype'=>$paytype,
        'date'=>$date,
        'medicinename'=>$medicinename,
        'quantity'=>$quantity,
        'mrp'=>$mrp,
        'amount'=>$amount,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    ];
    // print_r($_POST);
    $pdo = connectDB();
    // UPDATE `customer` SET `id`='[value-1]',`c_fname`='[value-2]',`c_lname`='[value-3]',`address`='[value-4]',`email`='[value-5]',`password`='[value-6]',`contactno`='[value-7]',`gender`='[value-8]' WHERE 1

$sql  ="UPDATE `purchase` SET 
                                `invoiceno` = :invoiceno,
                                `sname` = :sname,
                                `paytype`=:paytype,
                                `date`=:date,
                                `medicinename`=:medicinename,
                                `quantity`=:quantity,
                                `mrp`=:mrp,
                                `amount`=:amount,
                                `created_at`=:created_at,
                                `updated_at`=:updated_at
                                
                                WHERE `purchase`.`id` = :id";  
// echo $sql;
try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){
      $_SESSION['message'] = "Data is updated successfully";
      header("location:manage_purchase.php");
    }
  }catch(Exception $e){
      echo $e->getMessage();
    $_SESSION['message'] = "Data is NOT updated";
     //header("location:index.php");
  }  
?>

