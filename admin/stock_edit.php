<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');?>
<?php 

    $id = $_GET['id'];
    $data = ['id'=>$id];
    $pdo = connectDB();
    $sql = "SELECT * FROM `medicine` WHERE id = :id";

try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){// true means: execution of query is successful
      $data = $stmt->fetch();

    }
  }catch(Exception $e){
    echo $e->getMessage();
  }
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

 

  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">New Stock</h1>
            <p>Add Stock</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
           <!-- Change End Plz -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
           <form action="/admin/stock_update_processor.php" method="post"    name="">
                <input type="hidden" name="id" value="<?php echo $id ?>">
            <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="medicinename"  class="text-black">Medicine Name <span class="text-danger">*</span></label>
                        <input name="medicinename" value="<?= $data['medicinename'] ?>" type="text" class="form-control" id="medicinename" >
                        </div>
                        <div class="col-md-6">
                            <label for="genericname" class="text-black">Generic Name <span class="text-danger">*</span></label>
                            <input name="genericname" value="<?= $data['genericname'] ?>" type="text" class="form-control" id="genericname" >
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="packing" class="text-black">Packing <span class="text-danger">*</span></label>
                            <input  name="packing" value="<?= $data['packing'] ?>" type="text" class="form-control" id="packing"placeholder="">
                        </div>
                        </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="quantity" class="text-black">Quantity <span class="text-danger">*</span></label>
                            <input name="quantity" value="<?= $data['quantity'] ?>"  type="text" class="form-control" id="quantity" placeholder="put your secret password">
                        </div>
                    </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Update</button>
                        </div>
                        </div>
                    </div>
            </form>
           </div>
       </div>
    
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
