<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$fname = $_POST['fname'];
$lname = $_POST['lname'];
$email = $_POST['email'];
$password = $_POST['password'];
$address = $_POST['address'];
$phonenumber = $_POST['phonenumber'];
$gender = $_POST['gender'];
$marital = $_POST['marital'];
$created_at = $_POST['created_at'];
$updated_at = $_POST['updated_at'];

$data = [
        'fname'=>$fname,
        'lname'=>$lname,
        'email'=>$email,
        'password'=>$password,
        'address'=>$address,
        'phonenumber'=>$phonenumber,
        'gender'=>$gender,
        'marital'=>$marital,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    ];
$pdo = connectDB();


$sql = "INSERT INTO `users`( `fname`,
                               `lname`,
                                `email`,
                                `password`,
                               `address`,
                                 `phonenumber`,
                                 `gender`,
                                 `marital`,
                                 `created_at`,
                                `updated_at`
                                 ) 
                            VALUES (
                                    :fname, 
                                    :lname,
                                    :email,
                                    :password,
                                    :address,
                                    :phonenumber, 
                                    :gender,
                                    :marital,
                                    :created_at,
                                    :updated_at)";

$result = insert($sql, $data);
if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
 redirect('manage_customer.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";

  //redirect('add_new_customer.php');

}
