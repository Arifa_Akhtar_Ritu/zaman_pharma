<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
$id = $_POST['id'];

// echo "<pre>";
// print_r($_POST);
// echo "</pre>";
// die();
$country = ($_POST['country']);
$fname = $_POST['fname'];
$lname = $_POST['lname'];
$address = $_POST['address'];
$email = $_POST['email'];
$phonenumber = $_POST['phonenumber'];
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");
//sanitization
$data = ['id'=>$id,
        'country'=>$country,
        'fname'=>$fname,
        'lname'=>$lname,
        'address'=>$address,
        'email'=>$email,
       'phonenumber'=>$phonenumber,
         'created_at'=>$created_at,
        'updated_at'=>$updated_at
        
    ];
    // print_r($_POST);
    $pdo = connectDB();
    // UPDATE `customer` SET `id`='[value-1]',`c_fname`='[value-2]',`c_lname`='[value-3]',`address`='[value-4]',`email`='[value-5]',`password`='[value-6]',`contactno`='[value-7]',`gender`='[value-8]' WHERE 1

$sql  ="UPDATE `billing_details` SET 
                                `country`=:country,
                                `fname` = :fname,
                                `lname` = :lname,
                                `address`=:address,
                                `email`=:email,
                                `phonenumber`=:phonenumber,
                                `created_at`=:created_at,
                                `updated_at`=:updated_at
                                
                                WHERE `billing_details`.`id` = :id";  

try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){
      $_SESSION['message'] = "Data is updated successfully";
      header("location:manage_billing_details.php");
    }
  }catch(Exception $e){
      echo $e->getMessage();
    $_SESSION['message'] = "Data is NOT updated";
     //header("location:index.php");
  }  
?>

