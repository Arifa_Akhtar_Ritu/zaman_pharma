<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$invoiceno = $_POST['invoiceno'];
$sname = $_POST['sname'];
$paytype = $_POST['paytype'];
$date = $_POST['date'];
$medicinename = $_POST['medicinename'];
$quantity = $_POST['quantity'];
$mrp = $_POST['mrp'];
$amount = $quantity * $mrp;
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");

$data = [
        'invoiceno'=>$invoiceno,
        'sname'=>$sname,
        'paytype'=>$paytype,
        'date'=>$date,
        'medicinename'=>$medicinename,
        'quantity'=>$quantity,
        'mrp'=>$mrp,
        'amount'=>$amount,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    ];
$pdo = connectDB();
//INSERT INTO `purchase` (`invoiceno`, `sname`, `paytype`, `date`, `medicinename`, `quantity`, `mrp`, `amount`) VALUES (NULL, 'anik', 'cash', '2022-04-06', 'napa', '200', '12', '40');

$sql = "INSERT INTO `purchase`( `invoiceno`,
                               `sname`,
                               `paytype`,
                                `date`,
                                `medicinename`,
                                 `quantity`,
                                 `mrp`,
                                 `amount`,
                                 `created_at`,
                                `updated_at`) 
                            VALUES (
                                    :invoiceno, 
                                    :sname,
                                    :paytype,
                                    :date,
                                    :medicinename,
                                    :quantity, 
                                    :mrp,
                                    :amount)";

$result = insert($sql, $data);




if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
 redirect('manage_purchase.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";

  //redirect('add_new_customer.php');

}