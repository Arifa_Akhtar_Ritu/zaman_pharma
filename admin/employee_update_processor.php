<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
$id = $_POST['id'];
$e_fname = $_POST['e_fname'];
$e_lname = $_POST['e_lname'];
$email = $_POST['email'];
$password = ($_POST['password']);
$address = $_POST['address'];
$contactno = $_POST['contactno'];
$gender = $_POST['gender'];
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");

//sanitization
$data = ['id'=>$id,
        'e_fname'=>$e_fname,
        'e_lname'=>$e_lname,
        'address'=>$address,
         'email'=>$email,
        'password'=>$password,
       'contactno'=>$contactno,
         'gender'=>$gender,
         'created_at'=>$created_at,
        'updated_at'=>$updated_at
        
    ];
    // print_r($_POST);
    $pdo = connectDB();
    // UPDATE `customer` SET `id`='[value-1]',`c_fname`='[value-2]',`c_lname`='[value-3]',`address`='[value-4]',`email`='[value-5]',`password`='[value-6]',`contactno`='[value-7]',`gender`='[value-8]' WHERE 1

// $sql  ="UPDATE `employee` SET 
//                                 `e_fname` = :e_fname,
//                                 `e_lname` = :e_lname,
//                                 `address`=:address,
//                                 `email`=:email,
//                                 `password`=:password,
//                                 `contactno`=:contactno,
//                                 `gender`=:gender,
//                                 `created_at`=:created_at,
//                                 `updated_at`=:updated_at,
                                
//                                 WHERE `employee`.`id` = :id";  
// echo $sql;
$sql  ="UPDATE `employee` SET 
                                `e_fname` = :e_fname,
                                `e_lname` = :e_lname,
                                `address`=:address,
                                `email`=:email,
                                `password`=:password,
                                `contactno`=:contactno,
                                `gender`=:gender,
                                 `created_at`=:created_at,
                                `updated_at`=:updated_at 
                                
                                WHERE `employee`.`id` = :id";  
try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){
      $_SESSION['message'] = "Data is updated successfully";
      header("location:manage_employee.php");
    }
  }catch(Exception $e){
      echo $e->getMessage();
    $_SESSION['message'] = "Data is NOT updated";
     //header("location:index.php");
  }  
?>

