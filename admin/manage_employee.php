<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');
  include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
  if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])){
    ?>
    <div>
        <?php
    echo $_SESSION['message'] ;
    $_SESSION['message'] = "";
    ?>        
</div>
    <?php
}
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage employee</h1>
            <p>Manage Existing employee</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
       <form action="" method="get">
       <div class="col-md-12 form-group form-inline">
          <label class="font-weight-bold" for="">Search :</label>
          <input type="text"  name="keyword" class="form-control" id="">
        <button type="submit" class="btn btn-success font-weight-bold" ><i class="fas fa-search"></i></button>
        </div>
      </form>
<?php 
$pdo = connectDb();
$sql = "SELECT * FROM employee";
$data = [];

if( array_key_exists('keyword', $_GET) && !empty($_GET['keyword'])){
  $keyword = $_GET['keyword'];
  $sql = "SELECT * FROM employee WHERE e_fname LIKE :kw OR `e_lname` LIKE :kw OR `address` LIKE :kw";
  $data = ['kw'=>'%'.$keyword.'%'];
}

$dataset = getAll($sql, $data);
?>
        <div class="col col-md-12 table-responsive">
          <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover">
                  <thead>
                      <tr>
                          <th style="width: 2%;">SL.</th>
                          <th style="width: 13%;">Employee Name</th>
                          <th style="width: 13%;">Contact Number</th>
                          <th style="width: 17%;">Address</th>
                          <th style="width: 13%;">Email</th>
                          <th style="width: 17%;">Password</th>
                          <th style="width: 15%;">Action</th>
                      </tr>
                  </thead>
                  <tbody id="employees_div">
                <?php
                  $counter = 0;
                  foreach($dataset as $data):
                    //echo "".$data['c_fname']."</a>"."<br />";
                    $counter++;

                      ?>
                      <tr>
                        <th scope="row"><?php echo $counter;?></th>
                        <td><?php echo $data['e_fname']." ".$data['e_lname'];?></td>
                        <td><?php echo $data['contactno'];?></td>
                        <td><?php echo $data['address'];?></td>
                        <td><?php echo $data['email'];?></td>
                        <td><?php echo $data['password'];?></td>
                        <td>
                        <a href='employee_show.php?id=<?php echo $data["id"] ?>'>Show</a> |
                        <a href='employee_edit.php?id=<?php echo $data["id"] ?>'>Edit</a> |
                        <a href='employee_delete.php?id=<?php echo $data["id"] ?>' onclick="return confirm('are you sure?')"> Delete </a>

                        </td>
                        </tr>
                      <?php
                      endforeach;
                      ?> 
                  </tbody>
              </table>
          </div>
        </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
