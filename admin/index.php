<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <?php include_once('./partials/header.php');?>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
<?php include_once('./partials/aside.php');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row -->
        <hr>
        <!-- Main row -->
        <div class="row mb-2">
          <!-- Left col -->
          
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php include_once('../admin/partials/total_medicine.php');?>
           
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php include_once('../admin/partials/total_employee.php');?>
           
          </div>
           <!-- ./col -->
           <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php include_once('../admin/partials/total_invoice.php');?>
            
          </div>
          <!-- ./col -->
      <?php if($_SESSION['authUser']['role'] == 'admin'){ ?>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php include_once('../admin/partials/total_stock.php');?>
            
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php include_once('../admin/partials/expiry.php');?>
            
          </div>
         
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php include_once('../admin/partials/total_sales.php');?>
            
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php include_once('../admin/partials/total_puchase.php');?>
          </div>
          <!-- right col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php include_once('../admin/partials/total_supplier.php');?>
            
          </div>
          <?php  } ?>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
