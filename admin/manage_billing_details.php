<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');
  include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
  if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])){
    ?>
    <div>
        <?php
    echo $_SESSION['message'] ;
    $_SESSION['message'] = "";
    ?>        
</div>
    <?php
}
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Billing Details Information</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
       <div class="col-md-12 form-group form-inline">
          <label class="font-weight-bold" for="">Search :&emsp;</label>
          <input type="text" class="form-control" id="" placeholder="Search billing details" onkeyup="searchbilling_details(this.value);">
        </div>

        <div class="col col-md-12">
          <hr class="col-md-12" style="padding: 0px; border-top: 2px solid  #02b6ff;">
        </div>
        <?php 
$pdo = connectDB();
$sql = "SELECT * FROM `billing_details`";



try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute();

    $dataset = $stmt->fetchAll();

  }catch(Exception $e){
      echo $e->getMessage();
  }


?>

        <div class="col col-md-12 table-responsive">
          <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover">
                  <thead>
                      <tr>
                          <th>SL.</th>
                          <th>Country</th>
                          <th>Customer Full Name</th>
                          <th >Address</th>
                          <th>Email</th>
                          <th>Phone Number</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody id="billing_details_div">
                  <?php
                  $counter = 0;
                  foreach($dataset as $data):
                    //echo "".$data['c_fname']."</a>"."<br />";
                    $counter++;

                      ?>
                      <tr>
                        <th scope="row"><?php echo $counter;?></th>
                        <td><?php echo $data['country'];?></td>
                        <td><?php echo $data['fname']." ".$data['lname'];?></td>
                        <td><?php echo $data['address'];?></td>
                        <td><?php echo $data['email'];?></td>
                        <td><?php echo $data['phonenumber'];?></td>
                        <td>
                        <!-- <a href='c_show.php?id=<?php echo $data["id"] ?>'>Show</a> | -->
                        <a href='billing_details_edit.php?id=<?php echo $data["id"] ?>'>Edit</a> |
                        <a href='billing_details_delete.php?id=<?php echo $data["id"] ?>' onclick="return confirm('are you sure?')"> Delete </a>

                        </td>
                        </tr>
                      <?php
                      endforeach;
                      ?> 
                  </tbody>
              </table>
          </div>
        </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
