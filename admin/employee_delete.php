<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$id = $_GET['id'];
$data = ['id'=>$id];
$pdo = connectDB();

$sql = "DELETE FROM `employee` WHERE `employee`.`id` = :id";

try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){
      $_SESSION['message'] = "Data is deleted successfully";
      header("location:manage_employee.php");
    }
  }catch(Exception $e){
    $_SESSION['message'] = "Data is NOT deleted.";
    header("location:manage_employee.php");
  
    // @TODO
    // : any error sshould go to log file in production environment
  }
  
  