<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$mname = $_POST['mname'];
$mfd = $_POST['mfd'];
$exd = $_POST['exd'];
$created_at = $_POST['created_at'];
$updated_at = $_POST['updated_at'];

$data = [
        'mname'=>$mname,
        'mfd'=>$mfd,
        'exd'=>$exd,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    ];
$pdo = connectDB();

//INSERT INTO `expiredate`(`id`, `mname`, `mfd`, `exd`) VALUES ('[value-1]','[value-2]','[value-3]','[value-4]')
$sql = "INSERT INTO `expiredate`( `mname`,
                               `mfd`,
                               `exd`,
                               `created_at`,
                                `updated_at`
                                ) 
                            VALUES ( 
                                    :mname,
                                    :mfd,
                                    :exd
                                    )";

$result = insert($sql, $data);
if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
 redirect('manage_expiry_date.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";



}