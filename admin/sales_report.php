<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
$startDate = date('Y-m-d h:i:s', time());
$endDate = date('Y-m-d h:i:s', time());
if(array_key_exists('startDate', $_GET) && !empty($_GET['startDate'])){
  $startDate = $_GET['startDate'];
  $endDate = $_GET['endDate'];
 
 }
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');
  if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])){
    ?>
    <div>
        <?php
    echo $_SESSION['message'] ;
    $_SESSION['message'] = "";
    ?>        
</div>
    <?php
}
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Sales Report</h1>
            <p>Showing Sales Report</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
           <!-- Change End Plz -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
         <form action="" method="get">
        <div class="col-md-6 form-group form-inline">
            <label class="font-weight-bold" for="">Start Date</label>
            <input type="date" value="<?php echo $startDate?>" name="startDate" class="form-control" id="date" onchange="searchInvoice(this.value, 'INVOICE_DATE');">
        </div>
        <div class="col-md-6 form-group form-inline">
            <label class="font-weight-bold" for="">End Date</label>
            <input type="date" value="<?php echo $endDate?>" name="endDate" class="form-control" id="date" onchange="searchInvoice(this.value, 'INVOICE_DATE');">
            &emsp;
        </div>
       
        <div class="d-flex justify-content-between">
        &emsp;
        <button class="btn btn-success font-weight-bold" ><i class="fas fa-search"></i></button>
        <a href="sales_report_pdf.php"><button type="button" target="_bank" class="btn btn-success">PDF</button></a>
        </div>
</form>
        <?php 
$pdo = connectDB();

//$sql = "SELECT * FROM `invoice`";
// $sql = "SELECT * FROM `invoice` WHERE  created_at BETWEEN $startDate AND $endDate ";
$sql = "SELECT * FROM `orders` WHERE created_at BETWEEN :startDate AND :endDate ";
//$sql = "SELECT * FROM `orders` WHERE created_at >= :startDate AND created_at <= :endDate ";
$filterData = ['startDate'=>$startDate, 'endDate'=>$endDate];


try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($filterData);
    $dataset = $stmt->fetchAll();

   
  }catch(Exception $e){
      echo $e->getMessage();
  }


?>

            <div class="col col-md-12">
            <hr class="col-md-12" style="padding: 0px; border-top: 2px solid  #02b6ff;">
            
    </div>
       <div class="col col-md-12 table-responsive">
      <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover">
              <thead>
                  <tr>
                      <th>SL.</th>
                      <th>Sales Date</th>
                      <th>Customer NAME</th>
                      <th>Invoice No</th>
                      <th>Total Amount</th>
                      <th> Action</th>
                  </tr>
              </thead>
          <tbody id="sales_div">
          <?php
                  $counter = 0;
                  foreach($dataset as $data):
                    //echo "".$data['c_fname']."</a>"."<br />";
                    $counter++;

                      ?>
                      <tr>
                        <th scope="row"><?php echo $counter;?></th>
                        <td><?php echo date("F jS, Y", strtotime($data['created_at']));?></td>
                        <td><?php echo $data['fname']." ".$data['lname'];?></td>
                        <td><?php echo $data['invoice_id'];?></td>
                        <td><?php echo $data['item_sub_total_price'];?></td>
                        <td>
                        <a href='purchase_show.php?id=<?php echo $data["id"] ?>'>Show</a> |
                        <a href='purchase_edit.php?id=<?php echo $data["id"] ?>'>Edit</a> |
                        <a href='purchase_delete.php?id=<?php echo $data["id"] ?>' onclick="return confirm('are you sure?')"> Delete </a>

                        </td>
                        </tr>
                      <?php
                      endforeach;
                      ?> 
          </tbody>
          </table>
      </div>
</div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
