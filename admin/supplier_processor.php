<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$sfname = $_POST['sfname'];
$slname = $_POST['slname'];
$contactno = $_POST['contactno'];
$address = $_POST['address'];
$email = $_POST['email'];
$password = $_POST['password'];
$companyname = $_POST['companyname'];
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");

$data = [
        'sfname'=>$sfname,
        'slname'=>$slname,
        'contactno'=>$contactno,
        'address'=>$address,
        'email'=>$email,
        'password'=>$password,
        'companyname'=>$companyname,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    ];
$pdo = connectDB();
//INSERT INTO `suppliers` (`id`, `sfname`, `slname`, `contactno`, `address`, `email`, `password`, `companyname`) VALUES (NULL, 'anik', 'khan', '22222', 'uttara', 'ank@gmail.com', '1222', 'aci');

$sql = "INSERT INTO `employee`( `sfname`,
                               `slname`,
                               `contactno`,
                               `address`,
                                `email`,
                                `password`,
                                 `companyname`,
                                 `created_at`,
                                `updated_at`
                                 ) 
                            VALUES (
                                    :sfname, 
                                    :slname,
                                    :contactno,
                                    :address,
                                    :email,
                                    :password, 
                                    :companyname
                                    )";

$result = insert($sql, $data);
if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
 redirect('manage_supplier.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";

  //redirect('add_new_customer.php');

}