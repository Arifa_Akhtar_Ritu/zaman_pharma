<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');?>
<?php 

    $id = $_GET['id'];
    $data = ['id'=>$id];
    $pdo = connectDB();
    $sql = "SELECT * FROM `medicine` WHERE id = :id";

try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){// true means: execution of query is successful
      $data = $stmt->fetch();

    }
  }catch(Exception $e){
    echo $e->getMessage();
  }
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

 

  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Editing Medicine</h1>
            <p>Update Medicine</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
           <!-- Change End Plz -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
           <form action="/admin/medicine_update_processor.php" method="post"    name="" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <div class="p-3 p-lg-5 border">
                        <div class="form-group row">
                        <div class="col-md-6">
                            <label for="medicinename"  class="text-black">Medicine Name <span class="text-danger">*</span></label>
                            <input name="medicinename" value="<?= $data['medicinename'] ?>" type="text" class="form-control" id="medicinename" >
                        </div>
                        </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="genericname" class="text-black">Generic Name <span class="text-danger">*</span></label>
                            <input  name="genericname" value="<?= $data['genericname'] ?>" type="text" class="form-control" id="genericname"placeholder="">
                        </div>
                        </div>
                        <div class="form-group row">
                  <div class="col-md-12">
                    <label for="description" class="text-black">Description  </label>
                    <textarea type="text" class="form-control" id="description" name="description" value="<?= $data['description'] ?>" placeholder=""></textarea>
                  </div>
                </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="packing" class="text-black">Packing  <span class="text-danger">*</span></label>
                            <input  name="packing" value="<?= $data['packing'] ?>" type="text" class="form-control" id="packing" placeholder="">
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="quantity" class="text-black">Quantity  <span class="text-danger">*</span></label>
                            <input  name="quantity" value="<?= $data['quantity'] ?>" type="text" class="form-control" id="quantity" placeholder="">
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="price" class="text-black">Price <span class="text-danger">*</span></label>
                            <input  name="price" value="<?= $data['price'] ?>" type="digit" class="form-control" id="price" placeholder="">
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="price_unit" class="text-black">Unit <span class="text-danger">*</span></label>
                            <input  name="price_unit" value="<?= $data['price_unit'] ?>" type="digit" class="form-control" id="price_unit" placeholder="">
                        </div>
                        </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="supplier" class="text-black">Supplier <span class="text-danger">*</span></label>
                            <input  name="supplier" value="<?= $data['supplier'] ?>" type="text" class="form-control" id="supplier"placeholder="">
                        </div>
                        </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <!-- <label for="image" class="text-black">Image</label>
                            <input type="file" value="<?= $data['image'] ?>"  class="form-control" id="image" name="image" placeholder=""> -->
                            <label for="image"class="text-black">Image</label>
							    	<input name="image" type="file" class="form-control-file" id="image" >
                    <img src="<?php echo '/public/uploads'."/".$data['image']?>" width="100" height="100">
                          </div>
                    </div>
                        <div class="form-group row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Update</button>
                        </div>
                        </div>
                    </div>
            </form>
           </div>
       </div>
    
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
