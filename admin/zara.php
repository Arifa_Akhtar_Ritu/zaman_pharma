
2:43 PM
Zara
Zara Rahman
<?php
include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
$query= "SELECT * FROM orders";
$order_result = getAll($query);

?>
<!DOCTYPE HTML>
<html>
<?php include_once('./partials/head.php');?>
<body class="cbp-spmenu-push">
<div class="main-content">
<?php include_once('./partials/header.php');?>
<?php include_once('./partials/sidebar.php');?>
        
<div id="page-wrapper">
			<div class="main-page">
				<div class="tables">
					<h3 class="title1">Payment</h3>
					
					
				
					<div class="table-responsive bs-example widget-shadow">
						<h4>Payments:</h4>
						<table class="table table-bordered">
							<thead> 
								<tr> 
									<th>#</th> 
									<th>Name</th> 
									<th>Number</th>
									<th>Action</th>
								</tr> 
							</thead> 
                            <tbody>
                                     <?php
                                     $query= "SELECT * FROM orders";
                                     $statement=$pdo->prepare($query);
                                     $statement->execute();
                                     $statement->setFetchMode(PDO::FETCH_OBJ);
                                     $order_result=$statement->fetchAll();
                                     if($result){
                                             foreach($order_result as $row){
                                                 ?>
                                                 <tr>
                                                    <td><?=$row->id;?></td>
                                                    <td><?=$row->fname;?></td>
                                                    <td><?=$row->phonenumber;?></td>
													<td><a href='/admin/invoice.php?invoice_id=<?php echo $row->invoice_id ?>'  class="btn btn-success">Invoice</a> </td>


                                                 </tr>
                                                 <?php
                                             }
                                     }else{
                                         ?>
                                         <tr>
                                             <td colspan="4">No record found</td>
                                         </tr>
                                         <?php
                                     }
                                     ?>

                                </tbody>

						
						
						</table> 
					</div>
				</div>
			</div>
		</div>
    </div>
    <!--footer-->
    <?php include_once('./partials/footer.php');?>
    <!--//footer-->
	<!-- Classie -->

</body>

</html>