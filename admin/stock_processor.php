<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$medicinename = $_POST['medicinename'];
$genericname = $_POST['genericname'];
$packing = $_POST['packing'];
$quantity = $_POST['quantity'];
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");

$data = [
        'medicinename'=>$medicinename,
        'genericname'=>$genericname,
        'packing'=>$packing,
        'quantity'=>$quantity,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    ];
$pdo = connectDB();
//INSERT INTO `stock`(`id`, `medicinename`, `genericname`, `packing`, `quqntity`) VALUES ('[value-1]','[value-2]','[value-3]','[value-4]','[value-5]')

$sql = "INSERT INTO `medicine`( `medicinename`,
                               `genericname`,
                               `packing`,
                                `quantity`,
                                `created_at`,
                                `updated_at`
                                  ) 
                            VALUES (
                                    :medicinename, 
                                    :genericname,
                                    :packing,
                                    :quantity,
                                    :created_at,
                                    :updated_at
                                    )";

$result = insert($sql, $data);
if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
 redirect('manage_stock.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";

  //redirect('add_new_customer.php');

}