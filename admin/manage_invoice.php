<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');
  include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
  if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])){
    ?>
    <div>
        <?php
    echo $_SESSION['message'] ;
    $_SESSION['message'] = "";
    ?>        
</div>
    <?php
}
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Invoice</h1>
            <p>Manage Existing Invoice</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
       <form action="" method="get">
       <div class="col-md-12 form-group form-inline">
          <label class="font-weight-bold" for="">Search :</label>
          <input type="text" value="keyword"  name="keyword" class="form-control" id="">
        <button type="submit" class="btn btn-success font-weight-bold" ><i class="fas fa-search"></i></button>

        </div>
      </form>

 <?php 
$pdo = connectDb();

// $sql = "SELECT `invoice_id` FROM `orders` WHERE 1;";
$sql = "SELECT * FROM orders";
$data = [];

if( array_key_exists('keyword', $_GET) && !empty($_GET['keyword'])){
  $keyword = $_GET['keyword'];
  $sql = "SELECT * FROM orders WHERE m_name LIKE :kw OR `fname` LIKE :kw OR `lname` LIKE :kw";
  $data = ['kw'=>'%'.$keyword.'%'];
}

$dataset = getAll($sql, $data);

?>
       <div class="col col-md-12 table-responsive">
      <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover">
              <thead>
                  <tr>
                      <th>SL.</th>
                      <th>Invoice No</th>
                      <th>Customer Name</th>
                      <th>Medicine Name</th>
                      <th>Quantity</th>
                      <th>Price</th>
              <th>Total Amount</th>
              <th>Date</th>
              <th>Action</th>
                  </tr>
              </thead>
          <tbody id="invoices_div">
          <?php
                  $counter = 0;
                  foreach($dataset as $invoiceData):
                    $counter++;

                      ?>
                      <tr>
                        <th scope="row"><?php echo $counter;?></th>
                        <td><?php echo $invoiceData['invoice_id'];?></td>
                        <td><?php echo $invoiceData['fname']." " .$invoiceData['lname'];?></td>
                        <td><?php echo $invoiceData['m_name'];?></td>
                        <td><?php echo $invoiceData['m_quantity'];?></td>
                        <td><?php echo $invoiceData['m_price'];?></td>
                        <td><?php echo $invoiceData['item_sub_total_price'];?></td>
                        <td><?php echo $invoiceData['created_at'];?></td>
                        <td><a href='invoice_show.php?invoice_id=<?php echo $invoiceData['invoice_id'] ?>'  class="btn btn-success">Invoice</a> </td>
                        </tr>
                      <?php
                      endforeach;
                      ?> 
          </tbody>
          </table>
      </div>
</div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
