<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
$pdo = connectDb();
$sql = "SELECT * FROM suppliers";
$dataset = getAll($sql);
$supplierName =$dataset[0]['sfname'].' '.$dataset[0]['sfname'];
// dd($supplierName);
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">New Medicine</h1>
            <p>Add Medicine</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
           <!-- Change End Plz -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
           <form action="medicine_processor.php" method="post" enctype="multipart/form-data">
              <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                  <div class="col-md-6">
                    <label for="medicinename" class="text-black">Medicine Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="medicinename" name="medicinename">
                  </div>
                  <div class="col-md-6">
                    <label for="genericname" class="text-black">Generic Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="genericname" name="genericname">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="description" class="text-black">Description  </label>
                    <textarea type="text" class="form-control" id="description" name="description" placeholder=""></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="packing" class="text-black">Packing  </label>
                    <input type="text" class="form-control" id="packing" name="packing" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="quantity" class="text-black">Quantity  </label>
                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="price" class="text-black">Price</label>
                    <input type="digit" class="form-control" id="price" name="price" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="price_unit" class="text-black">Unit</label>
                    <input type="digit" class="form-control" id="price_unit" name="price_unit" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="supplier" class="text-black">Supplier</label>
                    <select class="form-control" name="supplier">
                     <?php foreach($dataset as $data){
                      ?>
                      <option value="<?php echo $data['sfname'].' '.$data['slname'];?>"><?php echo $data['sfname'].' '.$data['slname'];?></option>
                    <?php }?>

                    </select>
                    <!-- <input type="supplier" class="form-control" id="supplier" name="supplier" placeholder=""> -->
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="image" class="text-black">Image</label>
                    <input type="file" class="form-control" id="image" name="image" placeholder="">
                  </div>
                </div>
                
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                    </div>
                    </div>
                </div>
            </form>
           </div>
       </div>
    
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
