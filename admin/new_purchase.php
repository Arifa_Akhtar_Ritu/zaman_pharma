<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">New Purchase</h1>
            <p>Creating New Purchase</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
           <!-- Change End Plz -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
           <form action="purchase_processor.php" method="post">
              <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                  <div class="col-md-6">
                    <label for="invoiceno" class="text-black">Invoice No <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="invoiceno" name="invoiceno">
                  </div>
                  <div class="col-md-6">
                    <label for="sname" class="text-black">Supplier Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="sname" name="sname">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="paytype" class="text-black">pay Type <span class="text-danger">*</span></label>
                    <input type="paytype" class="form-control" id="paytype" name="paytype" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="date" class="text-black">Date <span class="text-danger">*</span></label>
                    <input type="date" class="form-control" id="date" name="date" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="medicinename" class="text-black">Medicine Name <span class="text-danger">*</span></label>
                    <input type="medicinename" class="form-control" id="medicinename" name="medicinename" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="quantity" class="text-black"> Quantity <span class="text-danger">*</span></label>
                    <input type="quantity" class="form-control" id="quantity" name="quantity" placeholder="">
                  </div> 
                  <div class="col-md-12">
     
                       <div class="form-outline">
                         <label class="form-label" for="mrp">MRP</label>
                         <input type="text" id="mrp" name="mrp" class="form-control form-control-lg" />
                       </div>
     
                     </div>
                 
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                    </div>
                    </div>
                </div>
            </form>
           </div>
       </div>
    
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
