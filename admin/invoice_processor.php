<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$invoice_id = $_POST['invoice_id'];
$fname = $_POST['fname'];
$lname = $_POST['lname'];
$date = $_POST['date'];
$mname = $_POST['mname'];
$quantity = $_POST['quantity'];
$price = $_POST['price'];
$totalamount =  $quantity * $price;
$totaldiscount =  $totalamount*(10/100);
$vat = $totalamount-$totaldiscount ;
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");



$data = [
        'invoice_id'=>$invoice_id,
        'fname'=>$fname,
        'lname'=>$lname,
        'date'=>$date,
        'mname'=>$mname,
        'quantity'=>$quantity,
        'price'=>$price,
        'totalamount'=>$totalamount,
        'totaldiscount'=>$totaldiscount,
        'vat'=>$vat,
        'created_at'=>$created_at,
        'updated_at'=>$updated_at
    ];
$pdo = connectDB();

//INSERT INTO `invoices` (`id`, `cname`, `date`, `totalamount`, `totaldiscount`, `netbil`) VALUES (NULL, 'ritu', '2022-04-05', '245', '20', '225');
$sql = "INSERT INTO `billing_details`( 
                                `invoice_id`, 
                                `fname`, 
                                `lname`, 
                                `date`, 
                                `mname`, 
                                `quantity`,`price`, 
                                `totalamount`,
                                 `totaldiscount`, 
                                `vat`,
                                `created_at`,
                                `updated_at`
                                ) 
                            VALUES ( 
                                    :invoice_id,
                                    :fname,
                                    :lname,
                                    :date,
                                    :mname,
                                    :quantity,
                                    :price,
                                    :totalamount,
                                    :totaldiscount,
                                    :vat,
                                    :created_at,
                                    :updated_at
                                    )";

$result = insert($sql, $data);
if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
 redirect('manage_invoice.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";



}