<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');
  include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
  if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])){
    ?>
    <div>
        <?php
    echo $_SESSION['message'] ;
    $_SESSION['message'] = "";
    ?>        
</div>
    <?php
}
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Medicine</h1>
            <p>Manage Existing Medicine</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
      <form action="" method="get">
       <div class="col-md-12 form-group form-inline">
          <label class="font-weight-bold" for="">Search :</label>
          <input type="text"  name="keyword" class="form-control" id="">
        <button type="submit" class="btn btn-success font-weight-bold" ><i class="fas fa-search"></i></button>

        </div>
      </form>
        <?php 
$pdo = connectDb();
$sql = "SELECT * FROM medicine";
$data = [];

if( array_key_exists('keyword', $_GET) && !empty($_GET['keyword'])){
  $keyword = $_GET['keyword'];
  $sql = "SELECT * FROM medicine WHERE medicinename LIKE :kw OR `genericname` LIKE :kw OR `description` LIKE :kw";
  $data = ['kw'=>'%'.$keyword.'%'];
}

$dataset = getAll($sql, $data);

?>
        <div class="col col-md-12">
          <hr class="col-md-12" style="padding: 0px; border-top: 2px solid  #02b6ff;">
        </div>

        <div class="col col-md-12 table-responsive">
          <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover">
                  <thead>
                      <tr>
                          <th>SL.</th>
                          <th>Medicine Name</th>
                          <th>Generic Name</th>
                          <th>Description</th>
                          <th>Packing</th>
                          <th>Quantity</th>
                          <th>Price</th>
                          <th>Supplier</th>
                          <th>Image</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody id="customers_div">
                  <?php
                  $counter = 0;
                  foreach($dataset as $data):
                    //echo "".$data['c_fname']."</a>"."<br />";
                    $counter++;

                      ?>
                      <tr>
                        <th scope="row"><?php echo $counter;?></th>
                        <td><?php echo $data['medicinename'];?></td>
                        <td><?php echo $data['genericname'];?></td>
                        <td><?php echo $data['description'];?></td>
                        <td><?php echo $data['packing'];?></td>
                        <td><?php echo $data['quantity'];?></td>
                        <td><?php echo $data['price'].$data['price_unit'];?></td>
                        <td><?php echo $data['supplier'];?></td>
                        <td><img src="<?php echo $uploads."/".$data['image']?>" width="100" height="100"></td>
                        <!-- <td><?php echo $data['image'];?></td> -->
                        <td>
                        <a href='medicine_show.php?id=<?php echo $data["id"] ?>'>Show</a> |
                        <a href='medicine_edit.php?id=<?php echo $data["id"] ?>'>Edit</a> |
                        <a href='medicine_delete.php?id=<?php echo $data["id"] ?>' onclick="return confirm('are you sure?')"> Delete </a>

                        </td>
                        </tr>
                      <?php
                      endforeach;
                      ?> 
                  </tbody>
              </table>
          </div>
        </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
