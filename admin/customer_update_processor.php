<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
$id = $_POST['id'];
$fname = $_POST['fname'];
$lname = $_POST['lname'];
$email = $_POST['email'];
$password = ($_POST['password']);
$address = $_POST['address'];
$phonenumber = $_POST['phonenumber'];
$gender = $_POST['gender'];
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");
//sanitization
$data = ['id'=>$id,
        'fname'=>$fname,
        'lname'=>$lname,
         'email'=>$email,
        'password'=>$password,
        'address'=>$address,
       'phonenumber'=>$phonenumber,
         'gender'=>$gender,
         'created_at'=>$created_at,
        'updated_at'=>$updated_at
        
    ];
    // print_r($_POST);
    $pdo = connectDB();
    // UPDATE `customer` SET `id`='[value-1]',`c_fname`='[value-2]',`c_lname`='[value-3]',`address`='[value-4]',`email`='[value-5]',`password`='[value-6]',`contactno`='[value-7]',`gender`='[value-8]' WHERE 1

$sql  ="UPDATE `users` SET 
                                `fname` = :fname,
                                `lname` = :lname,
                                `address`=:address,
                                `email`=:email,
                                `password`=:password,
                                `phonenumber`=:phonenumber,
                                `gender`=:gender,
                                `created_at`=:created_at,
                                `updated_at`=:updated_at,
                                
                                WHERE `users`.`id` = :id";  
// echo $sql;
try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){
      $_SESSION['message'] = "Data is updated successfully";
      header("location:manage_customer.php");
    }
  }catch(Exception $e){
      echo $e->getMessage();
    $_SESSION['message'] = "Data is NOT updated";
     //header("location:index.php");
  }  
?>

