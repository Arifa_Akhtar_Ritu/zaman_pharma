<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/config.php');
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$pdo = connectDb();


        $authUser = $_SESSION['authUser'];
        if(array_key_exists('id', $authUser) && !empty($authUser['id'])){
          
      $selectsql = "SELECT * FROM `purchase`";
      // $selectsql = "SELECT * FROM orders WHERE customer_id = :id";
        // $data = ['startDate'=>$startDate, 'endDate'=>$endDate];
         $data = ['id'=>$authUser['id']];

        }else{
          $selectsql = "SELECT * FROM purchase";
          $data = [];
        }
        $counter = 1;
        $stmt2 = $pdo->prepare($selectsql);
        $stmt2->execute($data);
        $stmt2->setFetchMode(PDO::FETCH_ASSOC);
        $invoiceData = $stmt2->fetchALL();

		$tr = '';
foreach ($invoiceData as $itemValue) :


	$counter++;
	$tr .= '<tr>';
	$tr .= '<td class="text-center">' . $counter . '</td>';
	$tr .= '<td>' . $itemValue['medicinename'] . '</td>';
	$tr .= '<td class="text-right">' . $itemValue['quantity'] . '</td>';
	$tr .= '<td class="text-right">' . $itemValue['mrp'] . '</td>';
	$tr .= '<td class="text-right">' . $itemValue['amount'] . ' BDT</td>';
	$tr .= '</tr>';
endforeach;


$htmloutput = <<<PDF
<h1>Purchase Report</h1>
<table style="border-collapse: collapse;width: 100%;" border="1">
			      <thead>
			        <tr style="border:1px solid">
			          <th class="text-center" style="width:5%">Ser No</th>
			          <th style="width:50%">Item</th>
			          <th class="text-right" style="width:15%">Quantity</th>
			          <th class="text-right" style="width:15%">Unit Price</th>
			          <th class="text-right" style="width:15%">Total Price</th>
			        </tr>
			      </thead>	
				  <tbody>
				  	$tr 
				  </tbody>			
         
			    </table>
PDF;

$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML($htmloutput);
$mpdf->Output();
