<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
$id = $_POST['id'];
$mname = $_POST['mname'];
$mfd = $_POST['mfd'];
$exd = $_POST['exd'];
$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");

//sanitization
$data = ['id'=>$id,
        'mname'=>$mname,
        'mfd'=>$mfd,
         'exd'=>$exd,
         'created_at'=>$created_at,
        'updated_at'=>$updated_at
        
    ];
    // print_r($_POST);
    $pdo = connectDB();
    //UPDATE `expiredate` SET `id`='[value-1]',`mname`='[value-2]',`mfd`='[value-3]',`exd`='[value-4]' WHERE 1

// $sql  ="UPDATE `expiredate` SET 
//                                 `mname` = :mname,
//                                 `mfd` = :mfd,
//                                 `exd`=:exd,
//                                 `created_at`=:created_at,
//                                 `updated_at`=:updated_at,
                                
//                                 WHERE `expiredate`.`id` = :id";  
// // echo $sql;
$sql  ="UPDATE `expiredate` SET 
                                `mname` = :mname,
                                `mfd` = :mfd,
                                `exd`=:exd,
                                `created_at`=:created_at,
                                `updated_at`=:updated_at 
                                
                                WHERE `expiredate`.`id` = :id";  
try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){
      $_SESSION['message'] = "Data is updated successfully";
      header("location:manage_expiry_date.php");
    }
  }catch(Exception $e){
      echo $e->getMessage();
    $_SESSION['message'] = "Data is NOT updated";
     //header("location:index.php");
  }  
?>

