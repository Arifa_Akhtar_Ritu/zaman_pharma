<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');
  include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

 
  
  
  $id = $_GET['id'];
  $data = ['id'=>$id];
  $pdo = connectDB();

  $sql = "SELECT * FROM `medicine` WHERE id = :id";

 
 
  try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){// true means: execution of query is successful
      $data = $stmt->fetch();
    //   print_r($data);

    }
  }catch(Exception $e){
    echo $e->getMessage();
  }
 

?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Medicine List</h1>
            <p>Medicine Data Show</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
       <div class="col-md-12 form-group form-inline">
          <label class="font-weight-bold" for="">Search :&emsp;</label>
          <input type="text" class="form-control" id="" placeholder="Search Medicine">
        </div>

        <div class="col col-md-12">
          <hr class="col-md-12" style="padding: 0px; border-top: 2px solid  #02b6ff;">
        </div>
        <div>
          <p> <strong>Medicine Name :</strong> <?php echo $data['medicinename'];?></p>
          <p> <strong>Generic Name :</strong><?php echo $data['genericname'];?></p>
          <p> <strong>Description :</strong><?php echo $data['description'];?></p>
          <p> <strong>Packing :</strong><?php echo $data['packing'];?></p>
          <p> <strong>Quantity :</strong><?php echo $data['quantity'];?></p>
          <p> <strong>Price :</strong><?php echo $data['price'].$data['price_unit'];?></p>
          <p> <strong>Supplier :</strong><?php echo $data['supplier'];?></p>
          <p> <strong>Image :</strong><img src="<?php echo $uploads."/".$data['image']?>" width="100" height="100"></p>
        </div>
   
     
     
       
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
