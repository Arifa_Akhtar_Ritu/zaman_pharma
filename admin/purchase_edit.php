<!DOCTYPE html>
<html lang="en">
<?php include_once('./partials/header.php');?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');?>
<?php 

    $id = $_GET['id'];
    $data = ['id'=>$id];
    $pdo = connectDB();
    $sql = "SELECT * FROM `purchase` WHERE id = :id";

try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){// true means: execution of query is successful
      $data = $stmt->fetch();

    }
  }catch(Exception $e){
    echo $e->getMessage();
  }
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

 

  <!-- Navbar -->
  <?php include_once('./partials/navigation.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include_once('./partials/aside.php');?>

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Purchase Data</h1>
            <p>Purchasing Data</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
           <!-- Change End Plz -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
           <form action="/admin/purchase_update_processor.php" method="post"    name="">
                <input type="hidden" name="id" value="<?php echo $id ?>">
            <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="invoiceno"  class="text-black">Invoice No <span class="text-danger">*</span></label>
                        <input name="invoiceno" value="<?= $data['invoiceno'] ?>" type="text" class="form-control" id="invoiceno" >
                        </div>
                            <div class="col-md-6">
                                <label for="sname" class="text-black">Supplier Name <span class="text-danger">*</span></label>
                                <input name="sname" value="<?= $data['sname'] ?>" type="text" class="form-control" id="sname" >
                            </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-6">
                        <label for="paytype"  class="text-black">Pay Type <span class="text-danger">*</span></label>
                        <input name="paytype" value="<?= $data['paytype'] ?>" type="text" class="form-control" id="paytype" >
                        </div>
                        <div class="col-md-6">
                            <label for="date" class="text-black">Date <span class="text-danger">*</span></label>
                            <input name="date" value="<?= $data['date'] ?>" type="date" class="form-control" id="date" >
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="medicinename" class="text-black">Medicne Name <span class="text-danger">*</span></label>
                            <input  name="medicinename" value="<?= $data['medicinename'] ?>" type="text" class="form-control" id="medicinename"placeholder="">
                        </div>
                        </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="quantity" class="text-black">Quantity <span class="text-danger">*</span></label>
                            <input name="quantity" value="<?= $data['quantity'] ?>"  type="text" class="form-control" id="quantity" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="mrp" class="text-black">MRP <span class="text-danger">*</span></label>
                            <input name="mrp" value="<?= $data['mrp'] ?>"  type="text" class="form-control" id="mrp" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="amount" class="text-black">Amount <span class="text-danger">*</span></label>
                            <input name="amount" value="<?= $data['amount'] ?>"  type="text" class="form-control" id="amount" placeholder="">
                        </div>
                    </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Update</button>
                        </div>
                        </div>
                    </div>
            </form>
           </div>
       </div>
    
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
