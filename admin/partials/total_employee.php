<?php 
    $sql = "SELECT count(*) as totalEmployee FROM employee";
    $result = get($sql);
?>
<div class="small-box bg-info">
              <div class="inner">
        <h3><?php echo $result['totalEmployee'];?></h3>
                <p>Manage Employee</p>
              </div>
              <div class="icon">
                <i class="right fas fa-angle-left"></i>
              </div>
              <a href="../admin/manage_employee.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>