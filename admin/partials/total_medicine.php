<?php 
    $sql = "SELECT count(*) as totalMedicines FROM medicine";
    $result = get($sql);
?>
<div class="small-box bg-warning">
    <div class="inner">
    <h3><?php echo $result['totalMedicines'];?></h3>
        <p>Total Medicine</p>
    </div>
    <div class="icon">
        <i class="ion ion-pie-graph"></i>
    </div>
    <a href="../admin/manage_medicine.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
</div>