<?php 
    $sql = "SELECT count(*) as totalExpiry FROM  expiredate";
    $result = get($sql);
?>
<div class="small-box bg-danger">
              <div class="inner">
              <h3><?php echo $result['totalExpiry'];?></h3>
                <p>Expiry Date</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="../admin/manage_expiry_date.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>