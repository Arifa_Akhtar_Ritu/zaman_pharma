
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/logo.png" alt="Admin Logo" class="brand-image img-elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo $_SESSION['authUser']['role'] == 'admin' ? 'Admin' : 'Employee' ?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <?php if($_SESSION['authUser']['role'] == 'admin'){ ?>

        <div class="image">
          <img src="dist/img/ritu.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <?php } ?>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION['authUser']['fname'] . ' ' .$_SESSION['authUser']['lname'] ?></a>
        </div>
       
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fas fa-balance-scale-left"></i>
                  <p>
                    Invoice
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <!-- <li class="nav-item">
                    <a href="/admin/invoice.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>New Invoice</p>
                    </a>
                  </li> -->
                  <li class="nav-item">
                    <a href="/admin/manage_invoice.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Manage Invoice</p>
                    </a>
                  </li>
                </ul>            
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fas fa-handshake"></i>
                  <p>
                    Customer
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/new_customer.php" class="nav-link">
                      
                      <i class="fal fa-balance-scale"></i>
                      <p>New Customer</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/manage_customer.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Manage Customer</p>
                    </a>
                  </li>
                </ul>            
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fas fa-balance-scale-left"></i>
                  <p>
                    Employee
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/new_employee.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>New Employee</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/manage_employee.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Manage Employee</p>
                    </a>
                  </li>
                </ul>            
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fas fa-capsules"></i>
                  <p>
                    Medicine
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/new_medicine.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>New Medicine</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/manage_medicine.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Manage Medicine</p>
                    </a>
                  </li>
                </ul>            
              </li>

              <?php if($_SESSION['authUser']['role'] == 'admin'){ ?>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fas fa-boxes"></i>
                  <p>
                    Stock
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/new_stock.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>New Stock</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/manage_stock.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Manage Stock</p>
                    </a>
                  </li>
                </ul>            
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-clock"></i>
                  <p>
                    Expiry Date
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/new_expiry.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>New Expiry Date</p>
                    </a>
                  </li>
                </ul>   
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/manage_expiry_date.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Manage Expiry Date</p>
                    </a>
                  </li>
                </ul>          
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fad fa-caravan"></i>
                  <p>
                    Supplier
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/new_supplier.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>New Supplier</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/manage_supplier.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Manage Supplier</p>
                    </a>
                  </li>
                </ul>            
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-chart-bar"></i>
                  <p>
                    Purchase
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/new_purchase.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>New Purchase</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/manage_purchase.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Manage Purchase</p>
                    </a>
                  </li>
                </ul>            
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fad fa-book-open"></i>
                  <p>
                    Report
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/sales_report.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Sales Report</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/purchase_report.php" class="nav-link">
                      <i class="fal fa-balance-scale"></i>
                      <p>Purchase Report</p>
                    </a>
                  </li>
              <?php } ?>

                </ul>            
              </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>