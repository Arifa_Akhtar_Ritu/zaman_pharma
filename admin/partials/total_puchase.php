<?php 
    $sql = "SELECT count(*) as totalPurchases FROM purchase";
    $result = get($sql);
    
?>
<div class="small-box bg-success">
     <div class="inner">
        <h3><?php echo $result['totalPurchases'];?></h3>
        <p>Total Purchase</p>
        </div>
        <div class="icon">
        <i class="ion ion-pie-graph"></i>
        </div>
     <a href="../admin/manage_customer.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
</div>