<?php 
    $sql = "SELECT count(*) as totalStock FROM medicine";
    $result = get($sql);
?>
<div class="small-box bg-warning">
              <div class="inner">
        <h3><?php echo $result['totalStock'];?></h3>
                <p>Stock</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="../admin/manage_stock.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>