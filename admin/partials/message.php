<?php 
  if(array_key_exists('message', $_SESSION ?? []) && !empty($_SESSION['message'])){
      $message_status = ($_SESSION['message_status'] == 'success')?'success':'danger';
      
?>
  <div class="alert alert-<?php echo $message_status;?>"><?php echo $_SESSION['message'] ?></div>
<?php
  $_SESSION['message'] = "";
  $_SESSION['message_status'] = "";
}
?>