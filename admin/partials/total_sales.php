<?php 
    $sql = "SELECT count(*) as totalSales FROM orders";
    $result = get($sql);
?>
<div class="small-box bg-success">
      <div class="inner">
        <!-- <h3>65</h3> -->
        <h3><?php echo $result['totalSales'];?></h3>
        <p>Total Sales</p>
      </div>
      <div class="icon">
        <i class="ion ion-pie-graph"></i>
      </div>
      <a href="../admin/purchase_report.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
</div>