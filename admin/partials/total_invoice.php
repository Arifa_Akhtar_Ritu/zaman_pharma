<?php 
    $sql = "SELECT count(*) as totalInvoices FROM invoices";
    $result = get($sql);
?>
<div class="small-box bg-info">
              <div class="inner">
        <h3><?php echo $result['totalInvoices'];?></h3>
             <p>Total Invoice</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="../admin/manage_invoice.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>