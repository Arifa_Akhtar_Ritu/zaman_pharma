<?php 
    $sql = "SELECT count(*) as totalSuppliers FROM suppliers";
    $result = get($sql);
?>
<div class="small-box bg-info">
    <div class="inner">
    <h3><?php echo $result['totalSuppliers'];?></h3>
      <p>Total Supplier</p>
    </div>
    <div class="icon">
        <i class="ion ion-pie-graph"></i>
    </div>
    <a href="../admin/manage_supplier.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
</div>