<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$pdo = connectDb();
$sql = "SELECT * FROM medicine";
$data = [];

if( array_key_exists('keyword', $_GET) && !empty($_GET['keyword'])){
  $keyword = $_GET['keyword'];
  $sql = "SELECT * FROM medicine WHERE medicinename LIKE :kw OR `genericname` LIKE :kw OR `description` LIKE :kw";
  $data = ['kw'=>'%'.$keyword.'%'];
}

$medicines = getAll($sql, $data);

?>
<!DOCTYPE html>
<html lang="en">

<?php include_once('partials/head.php') ?>


<body>

  <div class="site-wrap">

  <?php include_once('partials/header.php') ?>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="/frontend/index.php">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Store</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">

        <?php 
        if(count($medicines) > 0){
          foreach($medicines as $medicine){ 
        ?>
          <div class="col-sm-6 col-lg-4 text-center item mb-4">
            <a href="shop-single.php?id=<?php echo $medicine["id"] ?>"> <img src="<?php echo $uploads?>/<?php echo $medicine['image']?>" alt="Image" style="width: 18rem; height:18rem;"></a>
            <h3 class="text-dark"><a href="shop-single.php?id=<?php echo $medicine["id"] ?>"><?php echo $medicine['medicinename'] ?></a></h3>
            <p><?php echo"Generic Name : " . $medicine['genericname'] ?></p>
            <p><?php echo "Price : ". $medicine['price'].' '.$medicine['price_unit'] ?></p><p><?php echo "Qty : " .$medicine['quantity'] ?></p>
          </div>

          <?php 
          } 
        }else{
          ?>
<div> No record is found.</div>
          <?php
        }
        ?>


        </div>
       <!-- <div class="row mt-5"> 
          <div class="col-md-12 text-center">
            <div class="site-block-27">
              <ul>
                <li><a href="#">&lt;</a></li>
                <li class="active"><span>1</span></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&gt;</a></li>
              </ul>
            </div>
          </div>
        </div> -->

      </div>
    </div>

    
    <div class="site-section bg-secondary bg-image" style="background-image: url('images/bg_2.jpg');">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <a href="#" class="banner-1 h-100 d-flex" style="background-image: url('images/bg_1.jpg');">
              <div class="banner-1-inner align-self-center">
                <h2>Pharma Products</h2>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Molestiae ex ad minus rem odio voluptatem.
                </p>
              </div>
            </a>
          </div>
          <div class="col-lg-6 mb-5 mb-lg-0">
            <a href="#" class="banner-1 h-100 d-flex" style="background-image: url('images/bg_2.jpg');">
              <div class="banner-1-inner ml-auto  align-self-center">
                <h2>Rated by Experts</h2>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Molestiae ex ad minus rem odio voluptatem.
                </p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>


    <?php include_once('partials/footer.php') ?>
    
</body>

</html>