<?php
session_start();
echo "<pre>";
$id = $_GET['id'];
$data = ['id'=>$id];
print_r($data);
echo "</pre>";
$host = 'localhost';
$dbusername = 'root';
$dbpassword = "";
$db = "exercise";

$dsn = "mysql:host=$host;dbname=$db;charset=UTF8";

try {
	$pdo = new PDO($dsn, $dbusername, $dbpassword);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	if ($pdo) {
		echo "Connected to the $db database successfully!";
	}
} catch (PDOException $e) {
	  echo $e->getMessage();
} 
$sql = "SELECT * FROM `users` WHERE id = :id";

try{
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    if($result){// true means: execution of query is successful
      $data = $stmt->fetch();
     print_r($data);

    }
  }catch(Exception $e){
    echo $e->getMessage();
  }
?>

<!DOCTYPE html>
<html lang="en">
<?php include_once('partials/head.php') ?>

<body>
    <h1> Personal Details</h1>
    <?php
      if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])){
    ?>
    <div><?php 
    echo $_SESSION['message'] ;
    $_SESSION['message'] = "";
    
    ?> </div>
    <?php
}
    ?>
 <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <form action="/frontend/register_processor.php" method="post" name="">
             <input type="hidden" name="id" value="<?php echo $id ?>">
              <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                  <div class="col-md-6">
                    <label for="fname" class="text-black">First Name <span class="text-danger">*</span></label>
                    <input name="fname" type="text" class="form-control" id="fname" >
                  </div>
                  <div class="col-md-6">
                    <label for="lname" class="text-black">Last Name <span class="text-danger">*</span></label>
                    <input name="lname" type="text" class="form-control" id="lname" >
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="email" class="text-black">Email <span class="text-danger">*</span></label>
                    <input  name="email"  type="email" class="form-control" id="email"placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="password" class="text-black">Password <span class="text-danger">*</span></label>
                    <input name="password"  type="password" class="form-control" id="password" placeholder="put your secret password">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="role" class="text-black">Role <span class="text-danger">*</span></label>
                    <input name="role"  type="role" class="form-control" id="role" placeholder="put your role admin/customer/employee">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="address" class="text-black">Address <span class="text-danger">*</span></label>
                    <input name="address" type="address" class="form-control" id="address"  placeholder="">
                  </div> 
                  <div class="col-md-12">
     
                       <div class="form-outline">
                         <label for="phoneNumber"class="text-black">Phone Number</label>
                         <input name="phonenumber" type="tel" id="phoneNumber"  class="form-control form-control-lg" />
                       </div>
     
                     </div>
                  <br>
                  <div class="col-md-6">
                  <label for="gender" class="text-black">Gender</label>
            <input name="gender"  type="radio" id="male" value="male">
            <label for="male" class="text-black">Male</label>
            <input name="gender" type="radio" id="male"  value="female">
            <label for="female" class="text-black">Female</label>
                 </div>
                 <div class="col-md-6">
                 <label for="marital" class="text-black">Marital Status</label>
            <input  name="marital"  type="radio" id="married"value="married">
            <label for="married" class="text-black">Married</label>
            <input name="marital"  type="radio" id="single" value="single">
            <label for="single" class="text-black">Single</label>
                 </div>
                </div>
                <div class="form-group row">
                  <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                  </div>
                </div>
              </div>
            </form>
          </div>         
        </div>
      </div>
    </div>   
</body>
</html>
