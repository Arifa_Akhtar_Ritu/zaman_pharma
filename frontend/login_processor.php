<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$email = $_POST['email'];
$password = $_POST['password'];

// step1 - connecting to database
$pdo = connectDB();
// how to read data from database
//step 1 : prepare the query
$selectSql = "SELECT * FROM `users` WHERE  email = ? AND password = ?";
$stmt = $pdo->prepare($selectSql);
$data = [$email, $password];
$stmt->execute($data);

// step 2: fetch data
$stmt->setFetchMode(PDO::FETCH_ASSOC);
$data = $stmt->fetch();

if(!empty($data)){
    $_SESSION['message'] = "Login Successful.";
    $_SESSION['message_status'] = "success";
    $authUser = [
        'fname' => $data['fname'],
        'lname' => $data['lname'],
        'email' => $data['email'],
        'phonenumber' => $data['phonenumber'],
        'address' => $data['address'],
        'id' => $data['id'],
        'role' => $data['role']
    ];
    
    $_SESSION['authUser'] = $authUser;

    if($data['role'] == 'admin'){
        redirect('../admin/index.php');
    }else if($data['role'] == 'employee'){
        redirect('../admin/index.php');
    }
    else{
        redirect('index.php');
    }
}else{
    $_SESSION['message'] = "Credentials not found.";
    $_SESSION['message_status'] = "error";
    redirect('/frontend/register.php');
}



