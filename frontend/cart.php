<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

// $id = $_GET['id'];
// $data = ['id'=>$id];
$pdo = connectDb();

$sql = "SELECT * FROM `cart` WHERE customer_id = :customer_id";
$data = ['customer_id' => $unique_customer_id];

$stmt = $pdo->prepare($sql);
$stmt->execute($data);

$medicines = $stmt->fetchAll();


// echo "<pre>";
// print_r($medicines);
// echo "</pre>";
// die();

?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('partials/head.php') ?>
<?php include_once('partials/header.php') ?>



<body>

  <div class="site-wrap">
    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0">
            <a href="index.php">Home</a> <span class="mx-2 mb-0">/</span> 
            <strong class="text-black">Cart</strong>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row mb-5">
          <form action="add_to_cart_processor.php" class="col-md-12" method="post">
            <div class="site-blocks-table">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <!-- <th class="product-thumbnail">Medicine ID</th> -->
                    <th class="product-thumbnail">Medicine Image</th>
                    <th class="product-name">Medicine Name</th>
                    <th class="product-price">Medicine Price</th>
                    <th class="product-quantity">Quantity</th>
                    <th class="product-total">Total</th>
                    <th class="product-remove">Remove</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $counter = 0;
                    $subtotal =0;
                    foreach($medicines as $medicine):
                      $counter++;
                      $subtotal += $medicine['item_sub_total_price'];
                      ?>
                        <tr>
                    <td class="product-thumbnail">
                      <img src="<?php echo $uploads?>/<?php echo $medicine['m_image']?>" alt="Image" class="img-fluid">
                    </td>
                    <td class="product-name">
                      <h2 class="h5 text-black"><?php echo $medicine['m_name']?></h2>
                    </td>
                    <td><?php echo $medicine['m_price']?> BDT</td>
                    <td><?php echo $medicine['m_quantity']?></td>
                    <td><?php echo $medicine['item_sub_total_price']?> BDT</td>
                    
                    <td><a href="cart-remove.php?product_id=<?= $medicine['m_id'] ?>" class="btn btn-primary height-auto btn-sm">X</a></td>
                  </tr>
                  <?php
                      endforeach;
                      ?> 
                  
                </tbody>
              </table>
            </div>
          </form>
        </div>
    
        <div class="row">
         
          <div class="col-md-12 pl-5">
            <div class="row justify-content-end">
              <div class="col-md-7">
                <div class="row">
                  <div class="col-md-12 text-right border-bottom mb-5">
                    <h3 class="text-black h4 text-uppercase">Cart Totals</h3>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-md-6">
                    <span class="text-black">Subtotal</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black"><?php echo $subtotal?> BDT</strong>
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-6">
                    <span class="text-black">Total</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black"><?php echo $subtotal?> BDT</strong>
                  </div>
                </div>
    
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-primary btn-lg btn-block" onclick="window.location='checkout.php'">Proceed To
                      Checkout</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- <div class="site-section bg-secondary bg-image" style="background-image: url('images/bg_2.jpg');">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <a href="#" class="banner-1 h-100 d-flex" style="background-image: url('images/bg_1.jpg');">
              <div class="banner-1-inner align-self-center">
                <h2>Pharma Products</h2>
                <p>Paracetamol is a fast acting and safe analgesic with marked antipyretic property. It is specially suitable for patients who, for any reason, can not tolerate aspirin or other analgesics. Napa suppositories are used for rapid symptomatic management of pain and fever.

                </p>
              </div>
            </a>
          </div>
          <div class="col-lg-6 mb-5 mb-lg-0">
            <a href="#" class="banner-1 h-100 d-flex" style="background-image: url('images/bg_2.jpg');">
              <div class="banner-1-inner ml-auto  align-self-center">
                <h2>Rated by Experts</h2>
                <p>ExpertRating is a leader in Online Certification and Skill Testing and offers an affordable and efficient way for people to prove their expertise.
                </p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div> -->


    <?php include_once('partials/footer.php') ?>
    <script>
      let plusBtn = document.querySelector('.js-btn-plus');
      let minusBtn = document.querySelector('.js-btn-minus');

      plusBtn.addEventListener('click', function(){
        let qty = document.querySelector('#qty').value;
        let unitPrice = document.querySelector('#itemUnitPrice').innerText;
        document.querySelector('#itemTotalPrice').innerText = parseFloat(unitPrice) * ( parseFloat(qty) +1);
      });

      minusBtn.addEventListener('click', function(){
        let qty = document.querySelector('#qty').value;
        let updateQty = parseFloat(qty) - 1;
        if(updateQty < 1){
          alert('Invalid qty');
          return false;
        }
        let unitPrice = document.querySelector('#itemUnitPrice').innerText;
        document.querySelector('#itemTotalPrice').innerText = parseFloat(unitPrice) *  updateQty;
      });
    </script>
</body>

</html>