<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');?>
<!DOCTYPE html>




<?php 

if(!isset($_SESSION['authUser']) && empty($_SESSION['authUser']['id'])){
	echo "Please Login First";
	die();
}

$pdo = connectDb();

$sql = "SELECT * FROM `orders` WHERE user_id = :user_id GROUP BY invoice_id ORDER BY invoice_id DESC";

$stmt = $pdo->prepare($sql);
$stmt->execute(['user_id' => $_SESSION['authUser']['id']]);

$invoiceData = $stmt->fetchAll();

if(!$invoiceData){
  echo "Data not Found";
  die();
}


$counter_s = 0;
$subtotal_s = 0;
$total_s = 0;
foreach($invoiceData as $value){
	$counter_s++;
	$subtotal_s += $value['item_sub_total_price'];
	$disCount = ($subtotal_s/100)*10;
	$netTotal =$subtotal_s-$disCount;
	$vat =($netTotal/100)*15;
	$total_s = ($netTotal+$vat);
}

$invoiceNumber = $invoiceData[0]['invoice_id'];
$createdAt = $invoiceData[0]['created_at'];
$address = $invoiceData[0]['address'];
$customerName = $invoiceData[0]['fname'].' '.$invoiceData[0]['lname'];
$customerPhoneNumber = $invoiceData[0]['phonenumber'];
$customerEmail = $invoiceData[0]['email'];
$paymentCreatedAt = $invoiceData[0]['created_at'];

?>








<html lang="en">
    <?php include_once('../frontend/partials/head.php');?>
    <?php include_once('../frontend/partials/header.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="col-md-8 offset-2">
              <div class="card mb-3">
                <div class="card-body">
                  

                <div class="row table-row">
				<table class="table table-striped">
			      <thead>
			        <tr>
			          <th class="text-center" style="width:10%">Ser No</th>
			          <th style="width:50%">Invoice No</th>
			        </tr>
			      </thead>
			      <tbody>
				 <?php
				  $counter = 0;
                    $subtotal =0;
					// $total = 0;
                    foreach($invoiceData as $itemValue):
                      $counter++;
                      $subtotal += $itemValue['item_sub_total_price'];
					  $disCount = ($subtotal/100)*10;
					  $netTotal =$subtotal-$disCount;
					  $vat =($netTotal/100)*15;
					  $total = ($netTotal+$vat);

                      ?>
			        <tr>
			          <td class="text-center"><?php echo $counter?></td>
			          <td>
                    <a href="customer_invoice_item.php?invoice_id=<?php echo $itemValue['invoice_id'] ?>">
                        #<?php echo $itemValue['invoice_id'] ?>
                    </a>
                </td>
			        </tr>
					<?php
                      endforeach;
                      ?> 
					
			       </tbody>
			    </table>

			</div>
                 
                </div>
              </div>
       </div>
        <!-- /.row (main row) -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('./partials/footer.php');?>
</body>
</html>
