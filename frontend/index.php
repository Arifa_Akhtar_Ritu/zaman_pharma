<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('partials/head.php') ?>


<body>

  <div class="site-wrap">
  <?php include_once('partials/header.php') ?>
  <?php include_once('partials/message.php') ?>
  <?php include_once('partials/banner.php') ?>



    <div class="site-section">
      <div class="container">
        <div class="row align-items-stretch section-overlap">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
            <div class="banner-wrap bg-primary h-100">
              <a href="#" class="h-100">
                <h5>Free <br> Shipping</h5>
                <p>
                  Zaman Pharma is the largest online medicine pharmacy in Dhaka, Bangladesh.
                  <strong> Zaman Pharma provides the free home delivery service within Dhaka city.</strong>
                </p>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
            <div class="banner-wrap h-100">
              <a href="#" class="h-100">
                <h5>Season <br> Sale 50% Off</h5>
                <p>
                 Cheap Prices. Always Sale. Large Selection. The Best Price.
                </p>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
            <div class="banner-wrap bg-warning h-100">
              <a href="#" class="h-100">
                <h5>Buy <br> A Gift Card</h5>
                <p>
                  Use Online. *Discounts are only applicable on payments made through Credit Card/ Debit Card/ UPI/ Wallets.
                  </p>
              </a>
            </div>
          </div>

        </div>
      </div>
    </div>



  <!-- <?php include_once('partials/popular.php') ?>
  <?php include_once('partials/new.php') ?> -->
    



    <div class="site-section bg-secondary bg-image" style="background-image: url('images/bg_2.jpg');">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <a href="#" class="banner-1 h-100 d-flex" style="background-image: url('images/images\ \(1\).jpg');">
              <div class="banner-1-inner align-self-center">
                <h2>Zaman Pharma Products</h2>
                <p>Medicine is the science and practice of caring for a patient, managing the diagnosis, prognosis, prevention, treatment, palliation of their injury or disease, and promoting their health.
                </p>
              </div>
            </a>
          </div>
          <div class="col-lg-6 mb-5 mb-lg-0">
            <a href="#" class="banner-1 h-100 d-flex" style="background-image: url('images/bg_2.jpg');">
              <div class="banner-1-inner ml-auto  align-self-center">
                <h2>Rated by Experts</h2>
                <p>After compiling expert witness fee data from more than 35,000 cases, we discovered that the average rate for initial case reviews for all expert witnesses is $356.
                </p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>


    <?php include_once('partials/footer.php') ?>


</body>

</html>