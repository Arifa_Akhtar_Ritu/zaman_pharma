<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$fname = $_POST['fname'];
$lname = $_POST['lname'];
$email = $_POST['email'];
$password = ($_POST['password']);
$role = ($_POST['role']);
$address = $_POST['address'];
$phonenumber = $_POST['phonenumber'];
$gender = $_POST['gender'];
$marital = $_POST['marital'];

$data = ['fname'=>$fname,
        'lname'=>$lname,
        'email'=>$email,
        'password'=>$password,
        'role'=>$role,
        'address'=>$address,
        'phonenumber'=>$phonenumber,
        'gender'=>$gender,
        'marital'=>$marital,
    
    ];

$pdo = connectDB();

$sql = "INSERT INTO `users` (`fname`,
                            `lname`, `email`, 
                            `password`,`role`,
                             `address`, 
                            `phonenumber`, `gender`, 
                            `marital`) 
                     VALUES (
                             :fname, 
                             :lname,
                             :email,
                             :password,
                             :role,
                             :address,
                             :phonenumber, 
                             :gender,
                             :marital
                             )";

$result = insert($sql, $data);
if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
  redirect('index.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";

  redirect('register.php');

}