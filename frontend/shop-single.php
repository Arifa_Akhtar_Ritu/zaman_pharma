<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$id = $_GET['id'];
$data = ['id'=>$id];
$pdo = connectDb();

$sql = "SELECT * FROM `medicine` WHERE id = :id";

$stmt = $pdo->prepare($sql);
$stmt->execute($data);

$medicines = $stmt->fetch();


?>
<!DOCTYPE html>
<html lang="en">

<?php include_once('partials/head.php') ?>


<body>

  <div class="site-wrap">
  <?php include_once('partials/header.php') ?>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="index.php">Home</a> <span class="mx-2 mb-0">/</span> <a
              href="shop.php">Store</a> <span class="mx-2 mb-0">/</span></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-5 mr-auto">
            <div class="border text-center">
            <img src="<?php echo $uploads?>/<?php echo $medicines['image']?>" alt="Image" class="img-fluid p-5" >
            </div>
          </div>
          <div class="col-md-6">
            <h2 class="text-black"><?php echo $medicines['medicinename']?></h2>
            <p><?php echo $medicines['description']?></p>
            

            <p><?php echo $medicines['price'].$medicines['price_unit']?></p>
            <?php if($medicines['quantity'] <= 0) { ?>
                <p class="text-danger">Out Of Stock</p>
            <?php } else{
              echo "<p class='text-success'>In Stock</p>";
            } ?>
            
         <form action="add_to_cart_processor.php" method="post">
            <div class="mb-5">
              <div class="input-group mb-3" style="max-width: 220px;">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-primary js-btn-minus" type="button">&minus;</button>
                </div>
                <input type="text" name="product_qty" class="form-control text-center" value="1" placeholder=""
                  aria-label="Example text with button addon" aria-describedby="button-addon1">
                <div class="input-group-append">
                  <button class="btn btn-outline-primary js-btn-plus" type="button">&plus;</button>
                </div>
              </div>
    
            </div>
            <div class="site-section">
                <div class="container">
                      <div class="form-group row">
                      <div class="col-mb-6">
                      <input type="hidden" name="id" value="<?php echo $medicines['id'];?>" />
                      </div>
                      <div class="col-mb-6">
                      <input type="hidden" name="medicinename" value="<?php echo $medicines['medicinename'];?>" />
                      </div>
                      <div class="col-mb-6">
                      <input type="hidden" name="image" value="<?php echo $medicines['image'];?>" />
                      </div>

                      <div class="col-mb-6">
                      <input type="hidden" name="price" value="<?php echo $medicines['price']?>" />
                      <input type="hidden" name="unit_price" value="<?php echo $medicines['price_unit']?>" />
                    </div>
                  </div>
                </div>
            </div>

            <p><button type="submit" class="buy-now btn btn-sm height-auto px-4 py-3 btn-primary">Add To Cart</button></p>
        </form>
            <!-- <div class="mt-5">
              <ul class="nav nav-pills mb-3 custom-pill" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                    aria-controls="pills-home" aria-selected="true">Ordering Information</a>
                </li>
            
              </ul>
              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                  <table class="table custom-table">
                    <thead>
                      <th>Quantity</th>
                      <th>Generic Name</th>
                      <th>Packing</th>
                      </thead>
                      <tbody>
                      <tr>
                      <td><?php echo $medicines['quantity'];?></td>
                        <td><?php echo $medicines['genericname'];?></td>
                        <td><?php echo $medicines['packing'];?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div> -->
           </div>
        </div>
      </div>
    </div>
 <?php include_once('partials/footer.php') ?>
</body>
</html>