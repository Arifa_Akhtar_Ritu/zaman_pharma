<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('partials/head.php') ?>


<body>

  <div class="site-wrap">
  <?php include_once('partials/header.php') ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 d-flex justify-content-between mb-0">
            <a href="index.php">Home</a> 
              <a href="<?php echo $webroot?>/pdfgenerator.php" target="_blank"> PDF </a>
            </div>
        </div>
      </div>
    </div>
    
    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <span class="icon-check_circle display-3 text-success"></span>
            <h2 class="display-3 text-black">Thank you!</h2>
            <p class="lead mb-5">You order was successfuly completed.</p>
   

            <p><a href="shop.php" class="btn btn-md height-auto px-4 py-3 btn-primary">Back to store</a></p>
          </div>
        </div>
      </div>
    </div>


    <?php include_once('partials/footer.php') ?>

</body>

</html>