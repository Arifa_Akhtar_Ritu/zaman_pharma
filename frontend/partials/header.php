
    <div class="site-navbar py-2">

<div class="search-wrap">
  <div class="container">
    <a href="#" class="search-close js-search-close"><span class="icon-close2"></span></a>
    <form action="#" method="post">
      <input type="text" class="form-control" placeholder="Search keyword and hit enter...">
    </form>
  </div>
</div>

<div class="container-fluid">
  <div class="d-flex align-items-center justify-content-between">
    <div class="logo">
      <div class="site-logo">
      <img src="../../admin/dist/img/logo.png" alt="Admin Logo" width="20%" height="25%" class="brand-image img-elevation-3" style="opacity: .8">
        <a href="index.php" class="js-logo-clone">Pharma</a>
      </div>
    </div>
    <div class="main-nav d-none d-lg-block">
      <nav class="site-navigation text-right text-md-center" role="navigation">
        <ul class="site-menu js-clone-nav d-none d-lg-block">
          <li class="active"><a href="/frontend/index.php">Home</a></li>
          <?php if(isset($_SESSION['authUser'])) {?>
            <li><a href="/frontend/shop.php">Store</a></li>
          <?php } ?>

          <li><a href="/frontend/about.php">About</a></li>
          <?php if(!isset($_SESSION['authUser'])) {?>
            <li><a href="/frontend/register.php">Register</a></li>
            <li><a href="/frontend/login.php">Login</a></li>
          <?php }else{ ?>
            <li><a href="/frontend/logout.php">Log Out</a></li>
          <?php } ?>

          <li><a href="/frontend/contact.php">Contact</a></li>
        </ul>
      </nav>
    </div>
    <div class="search">
    <?php include_once($docroot."/frontend/partials/search.php") ?>
    </div>
    <div class="icons">  
      <a href="cart.php" class="icons-btn d-inline-block bag">
        <span class="icon-shopping-bag"></span>
        <span class="number"></span>
      </a>
      
    <a href="shop.php" class="icons-btn d-inline-block js-search-open m-2"></a>      
    <?php if(isset($_SESSION['authUser'])) {?>
    <a href="customer_profile.php" class="icons-btn d-inline-block js-search-open m-2"><?php echo("{$_SESSION['authUser']['fname']} {$_SESSION['authUser']['lname']}") ?></a>      
     
    <?php } ?>
    </div>
  </div>
</div>
</div>