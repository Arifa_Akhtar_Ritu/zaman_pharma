<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');



$pdo = connectDb();

$sql = "SELECT * FROM `cart` WHERE customer_id = :customer_id";
$data = ['customer_id' => $unique_customer_id];

$stmt = $pdo->prepare($sql);
$stmt->execute($data);

$medicines = $stmt->fetchAll();

$sql = "SELECT * FROM `users` WHERE id = :user_id";
$data = ['user_id' => $_SESSION['authUser']['id']];
$stmt = $pdo->prepare($sql);
$stmt->execute($data);
$user = $stmt->fetch();


?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('partials/head.php') ?>


<body>

  <div class="site-wrap">

  <?php include_once('partials/header.php') ?>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0">
            <a href="index.php">Home</a> <span class="mx-2 mb-0">/</span>
            <strong class="text-black">Checkout</strong>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-12">
            <div class="bg-light rounded p-3">
              <?php if(!isLoggedIn()) { ?>
              <p class="mb-0">Returning customer? <a href="login.php" class="d-inline-block">Click here</a> to login</p>
              <a href="<?php echo $webroot?>/pdfgenerator.php" target="_blank"> PDF </a>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-5 mb-md-0">
            <h2 class="h3 mb-3 text-black">Billing Details</h2>
            <div class="p-3 p-lg-5 border">
        <form action="checkout_process.php">
        <div class="form-group row">
                        <div class="col-md-12">
                            <label for="country" class="text-black">Country <span class="text-danger">*</span></label>
                            <input name="country" type="text" value="Bangladesh" class="form-control" id="country"  placeholder="">
                        </div> 
                        </div>
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="fname" class="text-black">First Name <span class="text-danger">*</span></label>
                  <input type="text" value="<?= $user['fname'] ?>" class="form-control" id="fname" name="fname">
                </div>
                <div class="col-md-6">
                  <label for="lname" class="text-black">Last Name <span class="text-danger">*</span></label>
                  <input type="text" value="<?= $user['lname'] ?>" class="form-control" id="lname" name="lname">
                </div>
              </div>
    
              <div class="form-group row">
                <div class="col-md-12">
                  <label for="address" class="text-black">Address <span class="text-danger">*</span></label>
                  <input type="text" value="<?= $user['address'] ?>" class="form-control" id="address" name="address" placeholder="Street address">
                </div>
              </div>
    
              <div class="form-group row mb-5">
                <div class="col-md-6">
                  <label for="email" class="text-black">Email Address <span class="text-danger">*</span></label>
                  <input type="text" value="<?= $user['email'] ?>" class="form-control" id="email" name="email">
                </div>
                <div class="col-md-6">
                  <label for="phonenumber" class="text-black">Phone <span class="text-danger">*</span></label>
                  <input type="tel" value="<?= $user['phonenumber'] ?>" class="form-control" id="phonenumber" name="phonenumber" placeholder="Phone Number">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
              <div class="row mb-5">
              <div class="col-md-12">
                <h2 class="h3 mb-3 text-black">Your Order</h2>
                <div class="p-3 p-lg-5 border">
                  <table class="table site-block-order-table mb-5">
                    <thead>
                      <th>Product</th>
                      <th>Total</th>
                    </thead>
                    <tbody>
                      <?php $total = 0; ?>
                    <?php foreach($medicines as $medicine){ ?>
                      <?php 
                        $total += $medicine['m_price'] * $medicine['m_quantity'];  
                      ?>
                      <tr>
                        <td><?= $medicine['m_name'] ?> <strong class="mx-2">x</strong> <?= $medicine['m_quantity'] ?></td>
                        <td><?= $medicine['m_price'] ?> BDT</td>
                      </tr>
                      <?php } ?>
                        <td class="text-black font-weight-bold"><strong>Order Total</strong></td>
                        <td class="text-black font-weight-bold"><strong><?= $total ?> BDT</strong></td>
                      </tr>
                    </tbody>
                  </table> 
                  <div class="border mb-3">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="payment_method" value="online" id="flexRadioDefault1">
                              <label class="form-check-label" for="flexRadioDefault1">
                                Instant Pay
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="payment_method" value="cash-on-delivery" id="flexRadioDefault2" checked>
                              <label class="form-check-label" for="flexRadioDefault2">
                                Cash On Delivery
                              </label>
                            </div>
        
                        <div class="collapse" id="collapsecheque">
                          <div class="py-2 px-4">
                            <p class="mb-0">Make your payment directly into our bank account. Please use your Order ID as the
                              payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
                          </div>
                        </div>
                      </div>
        
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                          Place Order
                        </button>
                    </form>
                  </div>
    
                </div>
              </div>
            </div>
    
          </div>
        </div>
        </form>
        <!-- </form> -->
      </div>
    </div>
    

    <div class="site-section bg-secondary bg-image" style="background-image: url('images/bg_2.jpg');">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <a href="#" class="banner-1 h-100 d-flex" style="background-image: url('images/bg_1.jpg');">
              <div class="banner-1-inner align-self-center">
                <h2>Pharma Products</h2>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Molestiae ex ad minus rem odio voluptatem.
                </p>
              </div>
            </a>
          </div>
          <div class="col-lg-6 mb-5 mb-lg-0">
            <a href="#" class="banner-1 h-100 d-flex" style="background-image: url('images/bg_2.jpg');">
              <div class="banner-1-inner ml-auto  align-self-center">
                <h2>Rated by Experts</h2>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Molestiae ex ad minus rem odio voluptatem.
                </p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <?php include_once('partials/footer.php') ?>

</body>

</html>