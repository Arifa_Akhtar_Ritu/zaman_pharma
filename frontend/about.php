<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<?php include_once('partials/head.php') ?>


<body>

  <div class="site-wrap">


  <?php include_once('partials/header.php') ?>
    

    <div class="site-blocks-cover inner-page" style="background-image: url('images/about.jpg');">
      <div class="container">
        <div class="row">
          <div class="col-lg-7 mx-auto align-self-center">
            <div class=" text-center">
              <h1>About Us</h1>
              <p>As a young entrepreneur in the 70s, he always had the dream to go beyond the typical concept of a Pharmacy in Bangladesh. Ever since the journey began,Zaman Pharma stood out, as its first and foremost focus was honesty and customer satisfaction. Over the last 40+ years, Zaman Pharma Ltd has never compromised with this core value of its business strategy</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section bg-light custom-border-bottom" data-aos="fade">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-6">
            <div class="block-16">
              <figure>
                <img src="images/bg_1.jpg" alt="Image placeholder" class="img-fluid rounded">
                <a href="https://www.youtube.com/watch?v=XjhqU5L4fG0 class="play-button popup-vimeo><span
                    class="icon-play"></span></a>
    
              </figure>
            </div>
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-5">
    
    
            <div class="site-section-heading pt-3 mb-4">
              <h2 class="text-black">How We Started</h2>
            </div>
            <p>Opening a medical store is a good business idea if one plans to venture into business in India as the overall profit in this field is considerably high compared to others. Another advantage of the medical shop business is that economic crises do not easily affect this business field. Nevertheless, it is important to note that proper analysis is a must before initiating this business venture as it may not do as well as expected in the absence of proper planning.

            </p>
    
          </div>
        </div>
      </div>
    </div>

    

    <div class="site-section bg-light custom-border-bottom" data-aos="fade">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-6 order-md-2">
            <div class="block-16">
              <figure>
                <img src="images/hero_1.jpg" alt="Image placeholder" class="img-fluid rounded">
              </figure>
            </div>
          </div>
          <div class="col-md-5 mr-auto">
    
    
            <div class="site-section-heading pt-3 mb-4">
              <h2 class="text-black">We Are Trusted Company</h2>
            </div>
            <p class="text-black">There are many medicine delivery apps that can help you out with the medicines but if you are someone looking to buy high quality generic medicines with extensive discount.

              We provide medicine services to pan Dhaka with there high quality medicines country.
    
          </div>
        </div>
      </div>
    </div>
    
    <div class="site-section site-section-sm site-blocks-1 border-0" data-aos="fade">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="">
            <div class="icon mr-4 align-self-start">
              <span class="icon-truck text-primary"></span>
            </div>
            <div class="text">
              <h2>Free Shipping</h2>
              <p>Zaman Pharma is the largest online medicine pharmacy in Dhaka, Bangladesh.
              Zaman Pharma provides the free home delivery service within Dhaka city.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="100">
            <div class="icon mr-4 align-self-start">
              <span class="icon-refresh2 text-primary"></span>
            </div>
            <div class="text">
              <h2>Free Returns</h2>
              <p>If any problem in our medicine , then feel free to send it back.We provide you new one.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="200">
            <div class="icon mr-4 align-self-start">
              <span class="icon-help text-primary"></span>
            </div>
            <div class="text">
              <h2>Customer Support</h2>
              <p>Customer support is a range of customer services to assist customers in making cost effective and correct use of a product. It includes assistance in planning, installation, training, troubleshooting, maintenance, upgrading, and disposal of a product.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <div class="site-section bg-light custom-border-bottom" data-aos="fade">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 site-section-heading text-center pt-4">
            <h2>The Team</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-6 mb-5">
    
            <div class="block-38 text-center">
              <div class="block-38-img">
                <div class="block-38-header">
                  <img src="images/abba.jpg" alt="Image placeholder" class="mb-4">
                  <h3 class="block-38-heading h4">Abdul Motin</h3>
                  <p class="block-38-subheading">CEO/Co-Founder</p>
                </div>
                <div class="block-38-body">
                  <p> With his experience of almost 20 years, the young entrepreneur now oversees the administration of the entire business, including the accounting department, financial reporting and franchise management.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-6 mb-5">
            <div class="block-38 text-center">
              <div class="block-38-img">
                <div class="block-38-header">
                  <img src="images/ritu.jpg" alt="Image placeholder" class="mb-4">
                  <h3 class="block-38-heading h4">Arifa Akter Ritu</h3>
                  <p class="block-38-subheading">Co-Founder</p>
                </div>
                <div class="block-38-body">
                  <p> Her primary goal with the e-commerce facet of Lazz Pharma is to make sure the customer experience is efficient and timely. Being a specialist in human resources management, ritu oversees the technical infrastructure and systems integration across all of zaman Pharma, and wishes to move the business forward globally.</p>
                </div>
              </div>
            </div>
          </div>
          
          </div>
            </div>
          </div>
        </div>
      </div>


      <?php include_once('partials/footer.php') ?>
    
</body>

</html>