<!DOCTYPE html>
<html lang="en">

  <?php include_once('partials/head.php') ?>

</html>
<body>
<section class="content">
      <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
           <form action="#" method="post">
              <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                  <div class="col-md-6">
                    <label for="medicinename" class="text-black">Medicine Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="medicinename" name="medicinename">
                  </div>
                  <div class="col-md-6">
                    <label for="genericname" class="text-black">Generic Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="genericname" name="genericname">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="packing" class="text-black">Packing  </label>
                    <input type="text" class="form-control" id="packing" name="packing" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="quantity" class="text-black">Quantity  </label>
                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="supplier" class="text-black">Supplier</label>
                    <input type="supplier" class="form-control" id="supplier" name="supplier" placeholder="">
                  </div>
                </div>
                
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                    </div>
                    </div>
                </div>
            </form>
           </div>
       </div>
    
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
</body>
</html>
