<!DOCTYPE html>
<html lang="en">
<?php include_once('../frontend/partials/head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php include_once('../frontend/partials/header.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">New Stock</h1>
            <p>Add Stock</p>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/frontend/index.php">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
           <!-- Change End Plz -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
           <form action="#" method="post">
              <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                  <div class="col-md-6">
                    <label for="mname" class="text-black">Medicine Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="mname" name="mname">
                  </div>
                  <div class="col-md-6">
                    <label for="gname" class="text-black">Generic Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="gname" name="gname">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="packing" class="text-black">Packing </label>
                    <input type="packing" class="form-control" id="packing" name="packing" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="quantity" class="text-black">Quantity<span class="text-danger">*</span></label>
                    <input type="quantity" class="form-control" id="quantity" name="quantity" placeholder="">
                  </div>
                </div>
                
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                    </div>
                    </div>
                </div>
            </form>
           </div>
       </div>
    
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</div>
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('../frontend/partials/footer.php');?>
</body>
</html>
