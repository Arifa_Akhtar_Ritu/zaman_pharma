<!DOCTYPE html>
<html lang="en">

  <?php include_once('partials/head.php') ?>

<body>

  <div class="site-wrap">
  <?php include_once('partials/header.php') ?>
    <div class="bg-light py-3">
      </div>
    </div>
    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <form action="customer_processor.php" method="post">
              <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                  <div class="col-md-6">
                    <label for="c_fname" class="text-black">First Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="c_fname" name="c_fname">
                  </div>
                  <div class="col-md-6">
                    <label for="c_lname" class="text-black">Last Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="c_lname" name="c_lname">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="email" class="text-black">Email <span class="text-danger">*</span></label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="password" class="text-black">Password <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="put your secret password">
                  </div>
                </div>
                <div class="form-group row">
                <div class="col-md-12">
                    <label for="address" class="text-black">Address <span class="text-danger">*</span></label>
                    <input type="address" class="form-control" id="address" name="address" placeholder="">
                  </div> 
                  <div class="col-md-12">
     
                       <div class="form-outline">
                         <label class="form-label" for="contactno">Contact No</label>
                         <input name="contactno" type="tel" id="contactno" class="form-control form-control-lg" />
                       </div>
     
                     </div>
                  <br>
                  <div class="col-md-6">
                  <label for="gender" class="text-black">Gender</label>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male" class="text-black">Male</label>
            <input type="radio" id="male" name="gender" value="female">
            <label for="female" class="text-black">Female</label>
                 </div>
                </div>
                <div class="form-group row">
                  <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                  </div>
                </div>
              </div>
            </form>
          </div>         
        </div>
      </div>
    </div>
</body>
</html>