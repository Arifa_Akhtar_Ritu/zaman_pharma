<?php

include_once("../config.php");

// if(array_key_exists('anonymous_user', $_SESSION) && !empty($_SESSION['anonymous_user'])){
//     $unique_customer_id = $_SESSION['anonymous_user'];
// }

$authUser = $_SESSION['authUser'];

$pdo = connectDb();


// create invoice
$invoice_no = makeUniqueNumber(6);
$sql = "INSERT INTO invoices(invoice_no, user_id) VALUES({$invoice_no}, {$authUser['id']})";
$stmt = $pdo->prepare($sql);
$stmt->execute();


// carts information
$sql = "SELECT * FROM `cart` WHERE customer_id = :customer_id";
$data = ['customer_id' => $unique_customer_id];

$stmt = $pdo->prepare($sql);
$stmt->execute($data);

$carts = $stmt->fetchAll();


try{
    
    // insert carts into orders table
    foreach ($carts as $cart) {


        $data = ['m_id' => $cart['m_id'],
                'customer_id' => $cart['customer_id'],
                'user_id' => $_SESSION['authUser']['id'],
                'm_image' => $cart['m_image'],
                'm_name' => $cart['m_name'],
                'm_price' => $cart['m_price'],
                'unit_price' => $cart['unit_price'],
                'm_quantity' => $cart['m_quantity'],
                'item_sub_total_price' => $cart['item_sub_total_price'],
                'invoice_id' => $invoice_no,
                'payment_method' => $_GET['payment_method'],
                'country' => $_GET['country'],
                'fname' => $_GET['fname'],
                'lname' => $_GET['lname'],
                'address' => $_GET['address'],
                'email' => $_GET['email'],
                'phonenumber' => $_GET['phonenumber']
             
            ];
             
        $sql = "INSERT INTO `orders` (`m_id`, `customer_id`, `user_id`, `m_image`, `m_name`, `m_price`, `unit_price`, `m_quantity`, `item_sub_total_price`, `invoice_id`, `payment_method`,`country`, `fname`, `lname`, `address`, `email`, `phonenumber`) 
                         VALUES (
                                :m_id, 
                                 :customer_id,
                                 :user_id,
                                 :m_image,
                                 :m_name,
                                 :m_price,
                                 :unit_price,
                                 :m_quantity, 
                                 :item_sub_total_price,
                                 :invoice_id,
                                 :payment_method,
                                 :country,
                                 :fname,
                                 :lname,
                                 :address,
                                 :email,
                                 :phonenumber)";
    
        $result = insert($sql, $data);

        $m_sql = "UPDATE medicine SET quantity = quantity - {$data['m_quantity']}";
        $m_stmt = $pdo->prepare($m_sql);
        $m_stmt->execute();
    }
    header('Location:../../../invoice_generator.php');

}catch(Exception $e){
    var_dump($e->getMessage());
}

