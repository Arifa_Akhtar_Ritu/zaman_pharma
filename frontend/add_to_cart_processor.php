<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

$customer_id = $unique_customer_id;
$m_id = $_POST['id'];
$m_image = $_POST['image'];
$m_name = $_POST['medicinename'];
$m_price = $_POST['price'];
$price_unit = $_POST['unit_price'];
$m_quantity = $_POST['product_qty'];
$item_sub_total_price =round($m_price*$m_quantity);


$created_at = date("Y-m-d h:i:s");
$updated_at = date("Y-m-d h:i:s");


$pdo = connectDB();

// select if the same product is already in the cart for the same customer
 $sql = "SELECT m_quantity FROM cart where m_id= :product_id AND customer_id = :customer_id";
 $data = ['product_id'=>$m_id, 'customer_id'=>$customer_id];

 try{
 $stmt = $pdo->prepare($sql);
 $stmt->execute($data);
 $result = $stmt->fetch();
 
 if($result){
  $existingQty = $result['m_quantity'];
 }else{
  $existingQty = 0;
 }
}catch(Exception $e){
  echo $e->getMessage();

 }

// $qty = 0;

// // if any then update qty and total price


// if($existingQty == 0){
//  $sql = "SELECT m_quantity FROM cart where m_id =:m_id";
//  $data_qty = ['m_id=>m_id'];
//  $m_quantity = 0;
if ($existingQty == 0){

  
$data = [
  'customer_id'=>$customer_id,
  'm_id'=>$m_id,
  'm_image'=>$m_image,
  'm_name'=>$m_name,
  'm_price'=>$m_price,
  'price_unit'=>$price_unit,
  'm_quantity'=>$m_quantity,
  'item_sub_total_price'=>$item_sub_total_price,
  'created_at'=>$created_at,
  'updated_at'=>$updated_at

];

$sql = "INSERT INTO `cart` (  
                              `customer_id`,                                     
                            `m_id`, 
                            `m_image`, 
                              `m_name`,
                                `m_price`,
                                `unit_price`,
                                 `m_quantity`,
                                 `item_sub_total_price`,
                                 `created_at`, 
                                `updated_at`) 
                     VALUES (
		                      	:customer_id,
                             :m_id,  
                             :m_image,  
                             :m_name,
                             :m_price,
                             :price_unit,
                             :m_quantity, 
                             :item_sub_total_price, 
                             :created_at,
                             :updated_at
                             )";

 $result = insert($sql, $data);
 }else{// it means existing qty is NOT 0. product already taken in cart. we just need to update the qty and total price


 $sql = "UPDATE cart SET m_quantity = :m_quantity, item_sub_total_price = :item_sub_total_price WHERE m_id = :m_id AND customer_id = :customer_id";
$data = ['m_quantity'=>$m_quantity,'item_sub_total_price'=>$item_sub_total_price,'m_id'=>$m_id, 'customer_id'=>$customer_id];

  
 try{
  $stmt = $pdo->prepare($sql);
  $stmt->execute($data);
  
 }catch(Exception $e){
   echo $e->getMessage();
 
  }
}
 



if($result){
  $_SESSION['message'] = "Data is stored successfully";
  $_SESSION['message_status'] = "success";
  redirect('../frontend/cart.php');
}else{
  $_SESSION['message'] = "There is a problem storing data. Please try again later.";
  $_SESSION['message_status'] = "failed";

  redirect('../frontend/cart.php');

}