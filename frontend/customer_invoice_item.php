<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';


if(!isset($_SESSION['authUser']) && empty($_SESSION['authUser']['id'])){
	echo "Please Login First";
	die();
}


$pdo = connectDb();

$sql = "SELECT * FROM `orders` WHERE user_id = :user_id AND invoice_id = :invoice_id";

$stmt = $pdo->prepare($sql);
$stmt->execute(['user_id' => $_SESSION['authUser']['id'], 'invoice_id' => $_GET['invoice_id']]);

$invoiceData = $stmt->fetchAll();


$counter_s = 0;
$subtotal_s = 0;
$total_s = 0;
foreach($invoiceData as $value){
	$counter_s++;
	$subtotal_s += $value['item_sub_total_price'];
	$disCount = ($subtotal_s/100)*10;
	$netTotal =$subtotal_s-$disCount;
	$vat =($netTotal/100)*15;
	$total_s = ($netTotal+$vat);
}

$invoiceNumber = $invoiceData[0]['invoice_id'];
$createdAt = $invoiceData[0]['created_at'];
$address = $invoiceData[0]['address'];
$customerName = $invoiceData[0]['fname'].' '.$invoiceData[0]['lname'];
$customerPhoneNumber = $invoiceData[0]['phonenumber'];
$customerEmail = $invoiceData[0]['email'];
$paymentCreatedAt = $invoiceData[0]['created_at'];

?>
<!doctype html>
<html lang="en">
<?php include_once('../frontend/partials/pdf_head.php');?>
<body>
<div class="container bootstrap snippets bootdeys">
   <div class="row">
       <div class="col-sm-12">
	  	   <div class="panel panel-default invoice" id="invoice">
		       <div class="panel-body">
			        <div class="invoice-ribbon"><div class="ribbon-inner">PAID</div></div>
		            <div class="row">

				<div class="col-sm-4 top-left">
					<!-- <i class="fa fa-rocket"></i> -->
					<img src="../admin/dist/img/logo.png" width="100px" height="100px">
				</div>
                <div class="col-md-4 top-middle">
                <h1> Zaman Pharmacy</h1>
                </div>
				<div class="col-sm-4 top-right">
						<h3 class="marginright">INVOICE-#<?php echo $_GET['invoice_id'];?></h3>
                        
						<span class="marginright"><?php echo $createdAt;?></span>
			    </div>

			</div>
			<hr>
			<div class="row">
				<div class="col-sm-4 from">
					<p class="lead marginbottom">From : Zaman Pharmacy</p>
					<p>Address:</p>
					<p>House-35,Road-10,Sector-10,Uttara,Dhaka.</p>
					<p>Phone: 01608637314</p>
					<p>Email: arifaritu7@gmail.com</p>
				</div>

				<div class="col-sm-4 to">
					<p class="lead marginbottom">To :<strong> <?php echo $customerName;?></strong></p>
					<p>Address: <?php echo $address;?></p>
					<p>Phone: <?php echo $customerPhoneNumber;?></p>
					<p>Email: <?php echo $customerEmail;?></p>

			    </div>

			    <div class="col-sm-4 text-right payment-details">
					<p class="lead marginbottom payment-info">Payment details</p>
					<p>Date: <?php echo $paymentCreatedAt;?></p>
					<p>Total Amount: <?php echo $total_s;?> BDT</p>
			    </div>

			</div>

			<div class="row table-row">
				<table class="table table-striped">
			      <thead>
			        <tr>
			          <th class="text-center" style="width:5%">Ser No</th>
			          <th style="width:50%">Item</th>
			          <th class="text-right" style="width:15%">Quantity</th>
			          <th class="text-right" style="width:15%">Unit Price</th>
			          <th class="text-right" style="width:15%">Total Price</th>
			        </tr>
			      </thead>
			      <tbody>
				 <?php
				  $counter = 0;
                    $subtotal =0;
					// $total = 0;
                    foreach($invoiceData as $itemValue):
                      $counter++;
                      $subtotal += $itemValue['item_sub_total_price'];
					  $disCount = ($subtotal/100)*10;
					  $netTotal =$subtotal-$disCount;
					  $vat =($netTotal/100)*15;
					  $total = ($netTotal+$vat);

                      ?>
			        <tr>
			          <td class="text-center"><?php echo $counter?></td>
			          <td><?php echo $itemValue['m_name'];?></td>
			          <td class="text-right"> <?php echo $itemValue['m_quantity'];?> </td>
			          <td class="text-right"><?php echo $itemValue['m_price'];?> BDT</td>
			          <td class="text-right"><?php echo $itemValue['item_sub_total_price'];?> BDT</td>
			        </tr>
					<?php
                      endforeach;
                      ?> 
					
			       </tbody>
			    </table>

			</div>

			<div class="row">
			<div class="col-md-6 margintop">
				<p class="lead marginbottom">THANK YOU!</p>
					  
				<!-- <a href="./pdfgenerator.php" class="btn btn-success" id="invoice-print"><i class="fa fa-print"></i> Print Invoice</a> -->
			</div>
			<div class="col-md-6 text-right pull-right invoice-total" style="padding-left: 400px;">
					  <p>Subtotal : <?php echo $subtotal;?> BDT</p>
			          <p>Discount (10%) : <?php echo $disCount;?> BDT</p>
			          <p>VAT (15%) : <?php echo $vat;?> BDT</p>
			          <p>Total : <?php echo $total;?> BDT</p>
			</div>
			</div>
		<form action="pdf_processor.php" method="post">
            <div class="site-section">
                <div class="container">
                      <div class="form-group row">
                      <div class="col-mb-6">
                      <input type="hidden" name="invoice_id" value="<?php echo $invoiceData[0]['invoice_id'];?>" />
                      </div>
                      <div class="col-mb-6">
                      <input type="hidden" name="fname" value="<?php echo $invoiceData[0]['fname'];?>" />
					  <input type="hidden" name="lname" value="<?php echo $invoiceData[0]['lname'];?>" />
                      </div>
                      <div class="col-mb-6">
                      <input type="hidden" name="address" value="<?php echo $invoiceData[0]['address'];?>" />
                      </div>

                      <div class="col-mb-6">
                      <input type="hidden" name="email" value="<?php echo $invoiceData[0]['email'];?>" />
                      <input type="hidden" name="phone_number" value="<?php echo$invoiceData[0]['phonenumber'];?>" />
                    </div>
					<div class="col-mb-6">
                      <input type="hidden" name="item_name" value="<?php echo $invoiceData[0]['m_name'];?>" />
                      <input type="hidden" name="quantity" value="<?php echo $invoiceData[0]['m_quantity'];?>" />
                      <input type="hidden" name="item_price" value="<?php echo $invoiceData[0]['m_price'];?>" />
                      <input type="hidden" name="item_sub_total_price" value="<?php echo$invoiceData[0]['item_sub_total_price'];?>" />
                      <input type="hidden" name="total_price" value="<?php echo $total;?>" />
                      <input type="hidden" name="vat" value="<?php echo $vat;?>" />
                      <input type="hidden" name="disCount" value="<?php echo $disCount;?>" />
                    </div>
                  </div>
                </div>
            </div>
            <!-- <p><button type="submit" class="buy-now btn btn-sm height-auto px-4 py-3 btn-primary">Payment Done</button></p> -->
        </form>
		  </div>
		</div>
	</div>
</div>
</div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>