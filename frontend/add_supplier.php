<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Add New Supplier</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Rubik:400,700|Crimson+Text:400,400i" rel="stylesheet">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <style>
        .gradient-custom {
     /* fallback for old browsers */
     background: #f093fb;
   
     /* Chrome 10-25, Safari 5.1-6 */
     background: -webkit-linear-gradient(to bottom right, rgba(240, 147, 251, 1), rgba(245, 87, 108, 1));
   
     /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
     background: linear-gradient(to bottom right, rgba(240, 147, 251, 1), rgba(245, 87, 108, 1))
   }
   
   .card-registration .select-input.form-control[readonly]:not([disabled]) {
     font-size: 1rem;
     line-height: 2.15;
     padding-left: .75em;
     padding-right: .75em;
   }
   .card-registration .select-arrow {
     top: 13px;
   }
     </style>
   </head>
   <body>
     <section class="vh-100 gradient-custom">
       <div class="container py-5 h-100">
         <div class="row justify-content-center align-items-center h-100">
           <div class="col-12 col-lg-9 col-xl-7">
             <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
               <div class="card-body p-4 p-md-5">
                 <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Supplier Information</h3>
                 <form>
     
                   <div class="row">
                     <div class="col-md-6 mb-4">
     
                       <div class="form-outline">
                         <input type="text" id="firstName" class="form-control form-control-lg" />
                         <label class="form-label" for="firstName">First Name</label>
                       </div>
     
                     </div>
                     <div class="col-md-6 mb-4">
     
                       <div class="form-outline">
                         <input type="text" id="lastName" class="form-control form-control-lg" />
                         <label class="form-label" for="lastName">Last Name</label>
                       </div>
     
                     </div>
                   </div>
                   <div class="col-md-6 mb-4">
                   <div class="col-md-6 mb-4 d-flex align-items-center">
     
                    <div class="form-outline datepicker w-100">
                      <label for="address" class="form-label">Address</label>
                      <input
                        type="address"
                        class="form-control form-control-lg"
                        id="address"
                      />
                    </div>
  
                  </div>
     
                   <div class="row">
                     <div class="col-md-6 mb-4">
     
                       <h6 class="mb-2 pb-1">Gender: </h6>
     
                       <div class="form-check form-check-inline">
                         <input
                           class="form-check-input"
                           type="radio"
                           name="inlineRadioOptions"
                           id="femaleGender"
                           value="option1"
                           checked
                         />
                         <label class="form-check-label" for="femaleGender">Female</label>
                       </div>
     
                       <div class="form-check form-check-inline">
                         <input
                           class="form-check-input"
                           type="radio"
                           name="inlineRadioOptions"
                           id="maleGender"
                           value="option2"
                         />
                         <label class="form-check-label" for="maleGender">Male</label>
                       </div>
     
                       <div class="form-check form-check-inline">
                         <input
                           class="form-check-input"
                           type="radio"
                           name="inlineRadioOptions"
                           id="otherGender"
                           value="option3"
                         />
                         <label class="form-check-label" for="otherGender">Other</label>
                       </div>
     
                     </div>
                   </div>
     
                   <div class="row">
                     <div class="col-md-6 mb-4 pb-2">
     
                       <div class="form-outline">
                         <input type="email" id="emailAddress" class="form-control form-control-lg" />
                         <label class="form-label" for="emailAddress">Email</label>
                       </div>
     
                     </div>
                     <div class="col-md-6 mb-4 pb-2">
     
                        <div class="form-outline">
                          <label class="form-label" for="password">Password</label>
                          <input type="password" id="password" class="form-control form-control-lg" />
                        </div>
      
                      </div>
                     <div class="col-md-6 mb-4 pb-2">
     
                       <div class="form-outline">
                         <input type="tel" id="phoneNumber" class="form-control form-control-lg" />
                         <label class="form-label" for="phoneNumber">Phone Number</label>
                       </div>
     
                     </div>
                   </div>
     
                   <div class="row">
                     <div class="col-12">
     
                       <select class="select form-control-lg">
                         <option value="1" disabled>Company Name</option>
                         <option value="2">Sqaure</option>
                         <option value="3">ACI</option>
                         <option value="4">Globe</option>
                         <option value="5">HealthCare</option>
                       </select>
                       <label class="form-label select-label">Choose Company</label>
     
                     </div>
                   </div>
     
                   <div class="mt-4 pt-2">
                     <input class="btn btn-primary btn-lg" type="submit" value="Submit" />
                   </div>
     
                 </form>
               </div>
             </div>
           </div>
           </div>
        </div>
         </div>
       </div>
     </section>
   </body>
   </html>