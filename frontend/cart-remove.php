<?php
include_once("../config.php");
if(array_key_exists('anonymous_user', $_SESSION) && !empty($_SESSION['anonymous_user'])){
    $unique_customer_id = $_SESSION['anonymous_user'];
}

$data = ['unique_customer_id' => $unique_customer_id, 'm_id' => $_GET['product_id']];

try{
    $sql = "DELETE FROM `cart` WHERE `cart`.`m_id` = :m_id AND `cart`.`customer_id` = :unique_customer_id";
    $pdo = connectDb();
    $stmt = $pdo->prepare($sql);
    $result = $stmt->execute($data);
    
    header('Location:cart.php');
    
}catch(Exception $e){
    dd($e->getMessage());
}


