<?php
ini_set('display_errors','On');
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

$docroot = $_SERVER['DOCUMENT_ROOT'];
$webroot = 'http://pharma.test';
$uploads = 'http://pharma.test/public/uploads';

$frontend = 'http://pharma.test/frontend';
$admin    = 'http://pharma.test/admin';


if(array_key_exists('anonymous_user', $_SESSION) && !empty($_SESSION['anonymous_user'])){
    $unique_customer_id = $_SESSION['anonymous_user'];
}else{
    $unique_customer_id = md5(time());
    $_SESSION['anonymous_user'] = $unique_customer_id;
}

function makeUniqueNumber($num){
    $unique_no = [];
    for($i=1; $i <= $num; $i++){
        $unique_no[] = rand(0, 9);
    }
    return implode($unique_no);
}
function redirect($url){
    header('location:'.$url);
}

function isLoggedIn(){
    if($_SESSION['authUser']){
        return true;
    }

    return false;
}

function d($data){  
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function dd($data){
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
    die();
}

function connectDb($displayMessage=false){
        
    $host = 'localhost';
    $dbusername = 'root';
    $dbpassword = "";
    $db = "exercise";

    $dsn = "mysql:host=$host;dbname=$db;charset=UTF8";

    try {
        $pdo = new PDO($dsn, $dbusername, $dbpassword);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if ($pdo) {
            if($displayMessage){
                echo "Connected to the $db database successfully!";
            }
            return $pdo;
        }

    } catch (PDOException $e) {
        echo $e->getMessage();
    } 

}
$pdo = connectDB();

function insert($sql, $data){
    try{
        $pdo = connectDB();
        $stmt = $pdo->prepare($sql);
        return $stmt->execute($data);
    }catch(Exception $e){
        echo $e->getMessage();
    }
}

// function getAllByAttr($query, $data)
function get($sql, $data=array()){
    $pdo = connectDB();
    try{
        $stmt = $pdo->prepare($sql);
        $result = $stmt->execute($data);
        if($result){// true means: execution of query is successful
          $data = $stmt->fetch(PDO::FETCH_ASSOC);
          //print_r($data);
          return $data;
        }
      }catch(Exception $e){
        echo $e->getMessage();
      }
}


function getAll($query, $data=array()){

    $pdo = connectDB();
    $statement=$pdo->prepare($query);
    $statement->execute($data);
    $statement->setFetchMode(PDO::FETCH_ASSOC);
    $result=$statement->fetchAll();

    return $result;
}


function update($sql, $data){
    try{
        $pdo = connectDB();
        $stmt = $pdo->prepare($sql);
        return $stmt->execute($data);
    }catch(Exception $e){
        echo $e->getMessage();
    }
}

function delete($sql, $data){
    try{
        $pdo = connectDB();
        $stmt = $pdo->prepare($sql);
        return $stmt->execute($data);
    }catch(Exception $e){
        echo $e->getMessage();
    }
}
